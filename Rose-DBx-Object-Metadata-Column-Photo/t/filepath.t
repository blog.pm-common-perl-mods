#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 2;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqaa', photo => 't/data/linux.png' );
$u->save();

ok( $u );

ok( -f 't/uploads/user/' . $u->photo );

$u->delete();
