package Metadata;

use strict;
use warnings;

use base 'Rose::DB::Object::Metadata';

__PACKAGE__->column_type_class(
    photo => 'Rose::DBx::Object::Metadata::Column::Photo'
);

1;
