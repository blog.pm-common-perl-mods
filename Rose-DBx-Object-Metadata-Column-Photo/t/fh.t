#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 5;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $fh = IO::File->new();
$fh->open( "< t/data/linux.png" );

my $u = User->new( name => 'qqaa', photo => $fh );
$u->save();

ok( $u );
ok( $u->photo );

my $newfile = 't/uploads/user/' . $u->photo;
ok( -d 't/uploads' );
ok( -f $newfile );

$u->delete();

ok( !-e 't/uploads' );

$fh->close();
