use inc::Module::Install;

name 'Rose-DBx-Object-Metadata-Column-Photo';

all_from 'lib/Rose/DBx/Object/Metadata/Column/Photo.pm';
perl_version '5.006';

requires(
    'Imager'           => '',
    'Data::UUID'       => '',
    'Rose::DB::Object' => '',
);

auto_install;

WriteAll;
