package Rose::DBx::Object::Metadata::Column::Photo;

use base 'Rose::DB::Object::Metadata::Column::Scalar';

use Rose::Object::MakeMethods::Generic ( scalar => 'directory' );

use File::Copy;
use Path::Class;
use File::Basename;
use Imager;
use Data::UUID;

sub type { return 'photo' }

sub init {
    my ( $self ) = shift;

    $self->add_builtin_trigger(
        event => 'deflate',
        code  => sub {
            handle_deflate( shift, $self );
        }
    );

    $self->SUPER::init( @_ );
}

sub _rootdir {
    my ( $object, $column ) = @_;

    return Path::Class::Dir->new(
        $object->can( 'directory' ) ? $object->directory : $column->directory );
}

sub handle_deflate {
    my ( $object, $column ) = @_;

    my $get_method = $column->accessor_method_name;
    my $set_method = $column->mutator_method_name;
    my $col_value  = $object->$get_method();

    if ( defined $col_value ) {
        _prepare_filedir( $object );

        my $img = Imager->new();

        my $input = ref $col_value ? 'fh' : 'file';
        eval { $img->read( $input => $col_value ) or die $img->errstr };
        return if $@;

        my $basename = _basename( $filename );
        my $filename = $basename . '.jpg';

        my $filedir = _filedir( $object );
        my $file     = $filedir->file( $filename );

        $img->write( file => $file, jpegquality => 80 );

        $object->$set_method( $filename );
    } else {
        $column->cleanup( $object );
        $object->$set_method( undef );
    }
}

sub cleanup {
    my ( $column, $object ) = @_;

    my $filedir = _filedir( $object );
    $filedir->file( $object->$column )->remove();

    $filedir->rmtree() unless -d $filedir && $filedir->children();

    my $rootdir = _rootdir( $object );
    $rootdir->rmtree() unless -d $rootdir && $rootdir->children();
}

sub _filedir {
    my ( $object ) = @_;

    return Path::Class::Dir->new( _rootdir( $object ), $object->meta->table );
}

sub _prepare_filedir {
    my ( $object ) = @_;

    _filedir( $object )->mkpath;

    die $! unless -d _filedir( $object );
}

sub _basename {
    my ( $filename ) = @_;

    my $ug = new Data::UUID;
    $uuid = $ug->create();

    return $ug->to_string( $uuid );
}

1;
