package CatalystX::Plugin::I18N::URI;

use strict;

sub uri_for {
    my ( $c, @subpath ) = @_; 

    if ( @subpath && $subpath[0] ne '/' ) { 
        @subpath = grep { defined $_ && $_ ne '' } @subpath;

        unless ( $subpath[ 0 ] && $subpath[ 0 ] =~ s/^\/+//xo ) { 
            my $action = $c->req->action;
            $action =~ s/^\/+//xo;
            unshift @subpath, $action;
        }

        unshift @subpath, ( '/' . $c->language );
    }   

    return $c->NEXT::uri_for( @subpath );
}

1;

