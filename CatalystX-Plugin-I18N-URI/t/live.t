use Test::More 'no_plan';
use lib 't/lib';
use Catalyst::Test 'TestApp';

is( get('/abs'), 'http://localhost/en/list', 'Absolute' );

is( get('/rel'), 'http://localhost/en/rel/add', 'Relative' );
