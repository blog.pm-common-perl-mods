package TestApp;

use Catalyst qw/ I18N +CatalystX::Plugin::I18N::URI /;

__PACKAGE__->config();

sub begin : Private {
    my ( $self, $c ) = @_;

    $c->languages( [ 'en' ] );
}

sub abs : Local {
    my ( $self, $c ) = @_;

    $c->res->body( $c->uri_for( '/list' ) );
}

sub rel : Local {
    my ( $self, $c ) = @_;

    $c->res->body( $c->uri_for( 'add' ) );
}

__PACKAGE__->setup();

1;
