package Common::Form;

use strict;

sub new {
    my ( $class ) = shift;

    my $self = bless { @_ }, $class;

    return $self;
}

sub was_submitted {
    my ( $self, $param ) = @_;

    $param ||= 'submit';

    return 1 if $self->{ $param };

    return 0;
}

sub _from_form {
    my ( $self ) = shift;
    my ( $object, $params ) = @_;

    while ( my ( $field, $value ) = each %$params ) {
        if ( $object->can( $field ) ) {
            $object->$field( $value );
        }
    }

    return $object;
}

sub create_from_form {
    my ( $self ) = shift;

    return $self->_from_form( @_ );
}

sub update_from_form {
    my ( $self ) = shift;

    return $self->_from_form( @_ );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
