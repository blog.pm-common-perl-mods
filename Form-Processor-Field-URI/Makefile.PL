use inc::Module::Install;

name 'Form-Processor-Field-URI';

all_from 'lib/Form/Processor/Field/URI.pm';

requires( 'Data::Validate::URI' => '', 'Form::Processor' => '' );

auto_install;

WriteAll;
