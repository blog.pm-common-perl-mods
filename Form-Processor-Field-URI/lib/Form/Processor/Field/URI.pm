package Form::Processor::Field::URI;
use strict;
use warnings;
use base 'Form::Processor::Field::Text';
our $VERSION = '0.03';
use Data::Validate::URI 'is_web_uri';


sub validate {
    my $self = shift;

    return unless $self->SUPER::validate;

    my $uri = $self->input;

    return $self->add_error('Enter a plain url "e.g. http://google.com"')
        unless is_web_uri( $uri );

    return 1;


}

1;
