use strict;
use warnings;
use lib './t';
use Test::More tests   => 4;

use Form::Processor::Field::URI;

my $field = Form::Processor::Field::URI->new(
    name    => 'test_field',
    type    => 'uri',
    form    => undef
);

ok( defined $field,  'new() called' );

$field->input( 'foo' );
$field->validate_field;
ok( $field->has_error );

$field->input( 'http://google.com' );
$field->validate_field;
ok( !$field->has_error );

$field->input( 'https://google.com' );
$field->validate_field;
ok( !$field->has_error );
