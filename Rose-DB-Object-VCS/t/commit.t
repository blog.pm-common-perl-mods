#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 12;

use lib 't/lib';

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my $post = Post->new(
    title   => 'Wow',
    content => 'Look there!',
    user_id => 1
);
$post->commit();

ok( $post->addtime );
is( $post->revision, 1 );
is( $post->user_id,  1 );
is( $post->title,    'Wow' );
is( $post->content,  'Look there!' );

$post = Post->new( id => $post->id );
$post->load();
is( $post->revision, 1 );

my $old_addtime = $post->addtime;
sleep( 1 );

$post->commit;
is( $post->revision, 1, 'Commit without changes' );

$post->title( $post->title );
$post->commit;
is( $post->revision, 1, 'Commit without changes' );

$post->title( 'Wuw' );
$post->user_id( 2 );
$post->commit();

isnt( $old_addtime, $post->addtime );

is( $post->revision, 2 );
is( $post->title,    'Wuw' );

is( scalar @{ $post->diffs }, 1 );

$post->delete( cascade => 1 );
