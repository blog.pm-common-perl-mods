#! /usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 6;

use lib 't/lib';

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my $post = Post->new(
    title   => 'Wow',
    content => 'Look there!',
    user_id => 1
);
$post->commit();
$post->title('Waw');
$post->commit();

$post = Post->new( id => $post->id );
$post->load();
$post->load_revision( 1 );
is( $post->title, 'Wow' );

$post = Post->new( id => $post->id );
$post->load();
$post->load_revision( 1 );
$post->user_id( 3 );
$post->commit();
is( $post->revision, 3 );
is( $post->title,    'Wow' );

$post = Post->new( id => $post->id );
$post->load();
$post->title( 'Woo' );
$post->user_id( 1 );
$post->commit();
is( $post->revision, 4 );
is( $post->title,    'Woo' );

$post->load_revision( 2 );
is( $post->title, 'Waw' );

$post->delete(cascade => 1);
