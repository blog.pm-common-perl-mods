package NewDB;

use DBI;

sub new {
    my $class = shift;

    my $self = { db => "/tmp/rose-vcs.db" };

    bless $self, $class;

    return $self;
}

sub init {
    my $self = shift;

    my $db = $self->{ db };

    unless ( -f $db ) {
        warn 'Creating new db...';

        my $dbh = DBI->connect( "dbi:SQLite:$db" ) or die $DBI::errstr;

        $dbh->do( <<SQL );
CREATE TABLE `posts` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `revision` INTEGER NOT NULL,
  `addtime` INTEGER NOT NULL,
  `user_id` INTEGER NOT NULL,
  `title` CHARACTER VARYING(255) NOT NULL,
  `content` CHARACTER VARYING(255) NOT NULL
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE `post_diffs` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `post_id` INTEGER NOT NULL,
  `revision` INTEGER NOT NULL,
  `user_id` INTEGER NOT NULL,
  `addtime` INTEGER NOT NULL,
  `title` CHARACTER VARYING(255),
  `content` CHARACTER VARYING(255)
);
SQL

        $dbh->disconnect();
    }
}

sub cleanup {
    my $self = shift;

    unlink $self->{ db };
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
