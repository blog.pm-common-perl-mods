package Post;

use strict;

use base qw(DB::Object);

use Rose::DB::Object::VCS qw(:all);

use Metadata;
sub meta_class { 'Metadata' }

__PACKAGE__->meta->setup(
    table => 'posts',

    columns => [
        qw/ id title content user_id addtime revision /,
    ],

    primary_key_columns => [ qw/ id / ],

    relationships => [
        diffs => {
            type       => 'one to many',
            class      => 'PostDiff',
            column_map => { id => 'post_id' }
        }
    ],

    versioned_columns => [ qw/ title content / ]
);

__PACKAGE__->meta->default_update_changes_only( 1 );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
