package PostDiff;

use strict;

use base qw(DB::Object);

__PACKAGE__->meta->setup(
    table => 'post_diffs',

    columns => [
        qw/ id post_id title content user_id revision addtime /,
    ],

    primary_key_columns => [ qw/ id / ],

    relationships => [
        post => {
            type       => 'many to one',
            class      => 'Post',
            key_columns => { post_id => 'id' }
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
