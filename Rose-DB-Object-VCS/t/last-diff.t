#! /usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 2;

use lib 't/lib';

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my $post = Post->new(
    user_id => 1,
    title => 'bu',
    content => 'how',
    user_id => 1
);
$post->commit();

$post = Post->new(id => $post->id);
$post->load();

my $diff = $post->last_diff();

ok( not defined $diff );

$post->title( 'hello' );
$post->commit();

$diff = $post->last_diff();

ok( $diff );

$post->delete(cascade => 1);
