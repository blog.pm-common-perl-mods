#! /usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 4;

use lib 't/lib';

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my $post = Post->new(
    user_id => 1,
    title => 'bu',
    content => 'how'
);
$post->commit();

$post->rollback();
is( $post->revision, 1 );

$post->title('haha');
$post->commit();

is( $post->title, 'haha' );

$post->rollback();
is( $post->revision, 3 );
is( $post->title, 'bu' );

$post->delete(cascade => 1);
