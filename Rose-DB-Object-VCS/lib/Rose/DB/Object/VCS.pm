package Rose::DB::Object::VCS;

use strict;

use Rose::Object::MixIn();
our @ISA = qw(Rose::Object::MixIn);

use Rose::DB::Object::Util qw(is_in_db has_modified_columns);

__PACKAGE__->export_tag(
    all => [ qw(commit load_revision rollback last_diff versioned_columns ) ] );

use Text::Diff();
use Text::Patch();

sub diff {
    my $self = shift;

    my @pk = $self->meta->primary_key_columns;
    my @pk_query = map { $_ => $self->$_ } @pk;

    my $initial = $self->meta->class->new( @pk_query );
    $initial->load();

    my ( $new, $old );
    my $data = {
        addtime  => $initial->addtime,
        user_id  => $initial->user_id,
        revision => $initial->revision
    };

    my $has_changed_columns = 0;
    my @versioned_columns   = $self->versioned_columns;
    foreach my $column ( @versioned_columns ) {
        $new = $self->$column || '';
        $new .= "\n";

        $old = $initial->$column || '';
        $old .= "\n";

        $data->{ $column } =
          Text::Diff::diff( \$new, \$old, { STYLE => 'Unified' } );

        $has_changed_columns++ if $data->{ $column };
    }

    return unless $has_changed_columns;

    return $data;
}

sub commit {
    my $self   = shift;
    my %params = @_;

    unless ( is_in_db( $self ) ) {
        $self->addtime( time ) unless $self->addtime;
        $self->revision( 1 );
        $self->save();
        return;
    }

    return unless has_modified_columns( $self );

    my $diff = diff( $self );

    return unless defined $diff;

    $self->add_diffs( $diff );

    $self->addtime( time ) unless $self->addtime;
    $self->revision( $self->revision + 1 );
    $self->save();
}

sub load_revision {
    my $self = shift;
    my ( $revision ) = @_;

    #print STDERR "\nCurrent revision is "
    #  . $self->revision
    #  . ", loading $revision\n";

    return
      if $self->revision == $revision
          || $self->revision < $revision
          || $revision <= 0;

    my @diffs = $self->find_diffs( sort_by => 'revision DESC' );

    my ( $src_column, $diff_column );
    foreach my $diff ( @diffs ) {

        #print STDERR "\nLoading " . $diff->revision, "\n";
        foreach my $column ( $self->versioned_columns ) {
            $src_column  = $self->$column;
            $diff_column = $diff->$column;

            if ( $diff_column ) {
                my $val =
                  Text::Patch::patch( $src_column, $diff_column,
                    { STYLE => 'Unified' } );
                $val =~ s/\n$//;
                $self->$column( $val );
            } else {
                $self->$column( $src_column );
            }
        }

        last if $diff->revision == $revision;
    }
}

sub rollback {
    my $self = shift;

    my $current_rev = $self->revision;

    return if $current_rev == 1;

    $self->load_revision( $current_rev - 1 );
    $self->commit();
}

sub last_diff {
    my $self = shift;

    my $current_rev = $self->revision;

    return if $current_rev == 1;

    return $self->find_diffs( query => [ revision => $current_rev - 1 ] );
}

sub versioned_columns {
    my $self = shift;

    my @columns;

    my @versioned_columns = $self->meta->versioned_columns;
    if ( @versioned_columns ) {
        @columns = @versioned_columns;
    } else {
        my @columns = $self->meta->column_names;

        my @pk = $self->meta->primary_key_columns;
        my $pk = join( '|', @pk );

        @columns = grep { $_ !~ m/^(?:revision|addtime|$pk)$/ } @columns;
    }

    return wantarray ? @columns : \@columns;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
