package Rose::DB::Object::VCS::Metadata;

use strict;
use warnings;

use base 'Rose::DB::Object::Metadata';

use Rose::Class::MakeMethods::Generic(
    scalar => [ qw/ versioned_columns / ] );

#sub versioned_columns {
    #my $self = shift;

    #$self->{ versioned_columns } = \@_ if @_;

    #return
      #wantarray
      #? @{ $self->{ versioned_columns } }
      #: $self->{ versioned_columns };
#}


1;
