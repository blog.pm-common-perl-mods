use strict;
use warnings;

use Test::More 'no_plan';

use lib 't/lib';
use Catalyst::Test qw/TestApp/;

my $res = get( '/?email=user@example.com' );

diag $res;

ok( $res );

ok( $res =~ m/gravatar_id/, 'gravatar_id' );

ok( $res =~ m/rating/, 'rating' );
