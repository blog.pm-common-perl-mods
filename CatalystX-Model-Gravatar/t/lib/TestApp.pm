package TestApp;

use warnings;
use strict;

use Catalyst::Runtime;

use Catalyst qw/ ConfigLoader /;

__PACKAGE__->config();

__PACKAGE__->setup;

1;
