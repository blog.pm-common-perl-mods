package TestApp::Model::Gravatar;

use strict;
use warnings;

use base 'CatalystX::Model::Gravatar';

__PACKAGE__->config( rating => 'R' );

1;
