package CatalystX::Model::Gravatar;

use strict;
use warnings;

use base 'Catalyst::Model';

use Gravatar::URL ();

sub new {
    my ( $class, $c ) = @_;

    my $self = $class->NEXT::new( $c );

    return $self;
}

sub url {
    my ( $self, $email ) = @_;

    return Gravatar::URL::gravatar_url( email => $email, %{ $self->config } );
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
