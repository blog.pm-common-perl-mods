package Common::Form::Processor::Model::RDBO::I18N;
use strict;
use warnings;
use Carp;
use Hash::Merge 'merge';
use base 'Form::Processor::Model::RDBO';

our $VERSION = '0.01';

sub build_form {
    my $self = shift;

    my $profile = merge $self->profile,
      {
        required => {
            orig_lang => {
                type    => 'Select',
                options => [
                    map { { value => $_ } } $self->object_class->i18n_languages
                ]
            }
        }
      };

    croak "Please define 'profile' method in subclass"
      unless ref $profile eq 'HASH';

    ### $$$ look at all keys in profile and allow keys to be Field names.

    for my $group ( qw/ required optional / ) {
        my $required = 'required' eq $group;

        $self->_build_fields( $profile->{ $group }, $required );

        my $auto_fields = $profile->{ 'auto_' . $group } || next;

        $self->_build_fields( $auto_fields, $required );
    }
}

sub update_from_form {
    my ( $self, $params ) = @_;

    my $item = $self->item;

    my $defined = defined $item ? 1 : 0;

    $params->{ orig_lang } = $item->orig_lang if $item;

    $item = $self->SUPER::update_from_form( $params );
    return unless $item;

    if ( not $defined ) {
        $item->orig_lang( $self->field( 'orig_lang' )->value );

        my $rel_name = $item->meta->i18n_translation_rel_name();

        my $i18n = $item->meta->relationship( $rel_name )->class->new();

        my $values = { istran => 0 };
        while ( my ( $field, $value ) = each %$params ) {
            if ( $i18n->can( $field ) ) {
                $values->{ $field } = $value;
            }
        }
        $item->$rel_name( $values );
    } else {
        my $i18n =
            $item->i18n
          ? $item->i18n
          : $item->i18n( $params->{ language } );

        return $item unless $i18n;

        while ( my ( $field, $value ) = each %$params ) {
            if ( $i18n->can( $field ) ) {
                $item->i18n->$field( $value );
            }
        }
    }

    return $item;
}

sub init_value {
    my ( $self ) = shift;
    my ( $field, $item ) = @_;

    my @values = $self->SUPER::init_value( @_ );

    return @values if scalar @values;

    my $column = $field->name;

    $item ||= $self->item;

    return unless $item->i18n;

    return
      unless $item
          && $item->isa( 'Rose::DB::Object' )
          && $item->i18n->can( $column );

    # @options can be a collection of RDBO objects (has_many) or a
    # RDBO objects get turned into IDs.  Should also check that it's not a compound
    # primary key.

    @values =
      map { ref $_ && $_->isa( 'Rose::DB::Object' ) ? $_->id : $_ }
      $item->i18n->$column;

    return @values;

}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 SEE ALSO

L<Form::Processor>

=cut

1;
