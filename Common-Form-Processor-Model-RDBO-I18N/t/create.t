use strict;
use Test::More tests => 4;

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $form = Form->new();

my $item = $form->update_from_form({ name => 'tom', orig_lang => 'en' });
ok( $item );
$item->save(cascade => 1);

my $u = User->new( name => 'tom' );
$u->load( speculative => 1 );
is( $u->not_found, 0 );

is( $u->orig_lang, 'en');
is( scalar @{ $u->user_i18n }, 3 );

$u->delete();
