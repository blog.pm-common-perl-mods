use strict;
use Test::More tests => 3;

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'foo', orig_lang => 'en' );
$u->save(cascade => 1);

my $form = Form->new( $u );

my $item = $form->update_from_form({ name => 'bar', signature => 'abc' });
ok( $item );
$item->save( cascade => 1 );

is( $u->name, 'bar' );
is( $u->i18n->signature, 'abc' );

$u->delete();
