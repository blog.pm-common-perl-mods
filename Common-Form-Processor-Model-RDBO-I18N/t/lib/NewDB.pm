package NewDB;

use DBI;

sub new {
    my $class = shift;

    my $self = { db => "/tmp/form-processor-model-rdbo-i18n.db" };

    bless $self, $class;

    return $self;
}

sub init {
    my $self = shift;

    my $db = $self->{ db };

    unless ( -f $db ) {
        warn 'Creating new db...';

        my $dbh = DBI->connect( "dbi:SQLite:$db" ) or die $DBI::errstr;

        $dbh->do( <<SQL );
CREATE TABLE `user` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `orig_lang` CHARACTER VARYING(2) NOT NULL,
  `name` CHARACTER VARYING(255) NOT NULL,
  UNIQUE (`name`)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE `user_i18n` (
  `i18nid` INTEGER PRIMARY KEY NOT NULL,
  `user_id` INTEGER NOT NULL,
  `lang` CHARACTER VARYING(2) NOT NULL,
  `signature` CHARACTER VARYING(255),
  `istran` TINYINT(1) NOT NULL DEFAULT 0
);
SQL

        $dbh->disconnect();
    }
}

sub cleanup {
    my $self = shift;

    unlink $self->{ db };
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
