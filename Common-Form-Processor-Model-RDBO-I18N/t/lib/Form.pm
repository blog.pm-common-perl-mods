package Form;

use strict;
use warnings;

use base 'Common::Form::Processor::Model::RDBO::I18N';

sub object_class { 'User' }

sub profile {
    my $self = shift;

    return {
        required => { name => 'Text' },
        unique   => [ qw/ name / ]
    };
}

1;
