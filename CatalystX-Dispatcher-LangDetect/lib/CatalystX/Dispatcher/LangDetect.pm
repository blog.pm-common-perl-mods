package CatalystX::Dispatcher::LangDetect;

use strict;

use base 'Catalyst::Dispatcher';

use I18N::LangTags::Detect;

sub prepare_action {
    my $self = shift;
    my ( $c ) = @_;

    my @path = split /\//, $c->req->path;

    my $languages = $c->config->{ languages };

    die 'Languages option should be a HASH of'
      . ' two_letters_language => original_language_name'
      unless ref $languages eq 'HASH';

    my @languages = keys %$languages;
    if ( $path[ 0 ] && grep { $path[ 0 ] eq $_ } @languages ) {
        my $language = $path[ 0 ];

        shift @path;
        $c->req->path( join( '/', @path ) );

        $c->languages( [ $language ] );
    } else {
        $c->log->debug(
            'Language is not specified in url. Trying to guess...' );
        my $language = $c->config->{ default_language } || 'en';

        $c->log->debug( 'Default language is ' . $language );

        my @browser_languages = I18N::LangTags::Detect::detect();
        $c->log->debug( 'User wants ' . join( ',', @browser_languages ) )
          if @browser_languages;

        foreach my $tag ( @browser_languages ) {
            if ( grep { $_ eq $tag } @languages ) {
                $language = $tag;
                last;
            }
        }

        $c->languages( [ $language ] );
    }

    $c->log->debug( 'Setting ' . $c->language . ' language' );

    $self->SUPER::prepare_action( @_ );
}

1;
