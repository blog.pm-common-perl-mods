use strict;
use warnings;

use Test::More;

BEGIN {
    plan 'no_plan';
}

use lib 't/lib';
use Catalyst::Test qw/TestApp/;

is( get( '/' ), 'en', 'get ok' );

is( get( '/hallo' ), 'en', 'get ok' );

is( get( '/ru/hallo' ), 'ru', 'get ok' );

$ENV{'HTTP_ACCEPT_LANGUAGE'} = 'ru';

is( get( '/hallo' ), 'ru', 'get ok' );
