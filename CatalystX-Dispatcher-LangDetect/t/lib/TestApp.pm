package TestApp;
use warnings;
use strict;

use Catalyst qw/ I18N /;

use Test::More;

sub default : Private {
    my ( $self, $c ) = @_;

    $c->res->body( $c->language );
}

__PACKAGE__->dispatcher_class( 'CatalystX::Dispatcher::LangDetect' );

__PACKAGE__->config->{ 'languages' } = {
    en => 'English',
    ru => 'Русский'
};

__PACKAGE__->setup;

1;
