package Form::Processor::Field::Fake;
use strict;
use warnings;
use base 'Form::Processor::Field';
our $VERSION = '0.01';

sub validate {
    my $self = shift;

    return unless $self->SUPER::validate;

    return $self->add_error( 'Do not fill in this field' )
      if defined $self->input;

    return 1;
}

=head1 AUTHORS

vti

=head1 COPYRIGHT

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
