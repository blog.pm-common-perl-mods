use strict;
use Test::More tests => 3;

use Form::Processor::Field::Fake;

my $field = Form::Processor::Field::Fake->new(
    name => 'test_field',
    type => 'fake',
    form => undef,
);

ok( defined $field, 'new() called' );

$field->input( 'blah' );
$field->validate_field;
ok( $field->has_error );

$field->input( '' );
$field->validate_field;
ok( not defined $field->has_error );
