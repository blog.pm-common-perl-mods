package TestApp::Model::User;

use strict;

use base qw/ Catalyst::Model::RDBO /;

__PACKAGE__->config( name => 'TestApp::RDBO::User' );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
