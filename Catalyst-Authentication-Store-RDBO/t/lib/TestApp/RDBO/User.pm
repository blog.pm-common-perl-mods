package TestApp::RDBO::User;

use strict;

use base qw(TestApp::DB::Object);

__PACKAGE__->meta->setup(
    table => 'user',

    columns => [ qw/id username email password status role_text session_data/ ],

    primary_key_columns => [ qw/ id / ],

    relationships => [
        roles => {
            type      => 'many to many',
            map_class => 'TestApp::RDBO::UserRole',
            map_from  => 'user',
            map_to    => 'role'
        },
        map_user_role => {
            type  => 'one to many',
            class => 'TestApp::RDBO::UserRole',
        },
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
