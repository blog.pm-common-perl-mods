package TestApp::RDBO::Role;

use strict;

use base qw(TestApp::DB::Object);

__PACKAGE__->meta->setup(
    table => 'role',

    columns => [ qw/id role/ ],

    primary_key_columns => [ qw/ id / ],
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
