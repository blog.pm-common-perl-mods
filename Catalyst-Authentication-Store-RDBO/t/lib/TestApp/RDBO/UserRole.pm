package TestApp::RDBO::UserRole;

use strict;

use base qw(TestApp::DB::Object);

__PACKAGE__->meta->setup(
    table => 'user_role',

    columns => [ qw/id user roleid/ ],

    primary_key_columns => [ qw/ id / ],

    foreign_keys        => [
        user => {
            class       => 'TestApp::RDBO::User',
            key_columns => { user => 'id' },
        },
        role => {
            class       => 'TestApp::RDBO::Role',
            key_columns => { roleid => 'id' },
        },
    ],
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
