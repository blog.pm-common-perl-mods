package Form::Processor::Field::SHA1_Password;
use strict;
use warnings;
use base 'Form::Processor::Field::Password';
use Digest::SHA 'sha1_hex';
our $VERSION = '0.01';



sub validate {
    my $self = shift;

    return unless $self->SUPER::validate;

    my $input = $self->input;

    return $self->add_error( 'Passwords must include one or more digits' )
        unless $input =~ /\d/;

    return 1;
}

sub input_to_value {
    my $field = shift;

    $field->value( sha1_hex( $field->input ) );

    return;
}



=head1 AUTHORS

vti

=head1 COPYRIGHT

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
