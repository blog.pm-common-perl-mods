use inc::Module::Install;

name 'Rose-DB-Object-Metadata-Column-Filename';

all_from 'lib/Rose/DB/Object/Metadata/Column/Filename.pm';
perl_version '5.006';

requires(
    'Rose::DB::Object' => '',

);

auto_install;

WriteAll;
