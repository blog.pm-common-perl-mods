package Rose::DB::Object::Metadata::Column::Filename;

use base 'Rose::DB::Object::Metadata::Column::Scalar';

use Rose::Object::MakeMethods::Generic ( scalar => 'directory' );

use File::Copy;
use Path::Class;
use File::Basename;
use Data::UUID;

sub type { return 'filename' }

sub init {
    my ( $self ) = shift;

    $self->add_builtin_trigger(
        event => 'deflate',
        code  => sub {
            handle_deflate( shift, $self );
        }
    );

    $self->SUPER::init( @_ );
}

sub _directory {
    my ( $object, $column ) = @_;

    return Path::Class::Dir->new(
        $object->can( 'directory' ) ? $object->directory : $column->directory );
}

sub handle_deflate {
    my ( $object, $column ) = @_;

    my $get_method = $column->accessor_method_name;
    my $set_method = $column->mutator_method_name;
    my $col_value  = $object->$get_method();
    my $dir        = _directory( $object, $column );

    my $destdir = _file_column_filedir( $object, $dir, $column->name );

    if ( defined $col_value ) {
        return unless ref $col_value eq 'HASH';
        my $filename = $col_value->{ filename };
        my $fh       = $col_value->{ fh };
        return unless $fh && ref $fh;

        _file_column_prepare_destdir( $destdir );

        my $basename = _file_column_basename( $filename );

        my $file = $destdir->file( $basename );
        File::Copy::copy( $fh, $file->stringify );

        $object->$set_method( $basename );
    } else {
        $column->cleanup( $object );
        $object->$set_method( undef );
    }
}

sub cleanup {
    my ( $column, $object ) = @_;

    my $dir     = _directory( $object, $column );
    my $destdir = _file_column_filedir( $object, $dir, $column->name );

    if ( -d $destdir ) {
        $destdir->rmtree();
        while ( ( $destdir = $destdir->parent )->absolute ne $dir->absolute ) {
            $destdir->rmtree() unless $destdir->children;
        }
        $destdir->rmtree() unless $destdir->children;
    }
}

sub _file_column_filedir {
    my ( $object, $dir, $col_name ) = @_;

    return Path::Class::Dir->new( $dir, $object->meta->table, $object->id,
        $col_name );
}

sub _file_column_prepare_destdir {
    my ( $path ) = @_;

    if ( -d $path ) {
        $path->rmtree;
    }

    $path->mkpath;

    die $! unless -d $path;
}

sub _file_column_basename {
    my ( $filename ) = @_;

    my $basename;
    if ( $filename =~ m/^[A-Z]:\\/ ) {
        my $type = File::Basename::fileparse_set_fstype();
        File::Basename::fileparse_set_fstype( 'MSWin32' );
        $basename = File::Basename::basename( $filename );
        File::Basename::fileparse_set_fstype( $type );
    } else {
        $basename = File::Basename::basename( $filename );
    }

    return $basename;
}

1;
