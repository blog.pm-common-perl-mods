#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 6;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqqq' );
$u->save();

ok( $u );

my $fh = IO::File->new();
$fh->open("< t/lib/User.pm");

$u->file({ fh => $fh, filename => 'Module.pm' });
$u->save();

is( $u->file, 'Module.pm' );
my $newfile = 't/uploads/user/' . $u->id . '/file/Module.pm';
ok( -f $newfile );
is( ( stat $newfile )[ 7 ], ( stat 't/lib/User.pm' )[ 7 ] );

$u->file(undef);
$u->save;

ok( not defined $u->file );

ok( !-f $newfile );

$u->delete();

$fh->close();
