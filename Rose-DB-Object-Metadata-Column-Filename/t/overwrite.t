#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 5;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqqq' );
$u->save();

ok( $u );

my $fh = IO::File->new();
$fh->open("< t/lib/User.pm");

$u->file({ fh => $fh, filename => 'Module.pm' });
$u->save();

is( $u->file, 'Module.pm' );
ok( -f 't/uploads/user/' . $u->id . '/file/Module.pm' );

$u->file({ fh => $fh, filename => 'Module1.pm' });
$u->save;

ok( -f 't/uploads/user/' . $u->id . '/file/Module1.pm' );
ok( !-f 't/uploads/user/' . $u->id . '/file/Module.pm' );

$u->delete();

$fh->close();
