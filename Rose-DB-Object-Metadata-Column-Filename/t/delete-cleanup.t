#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 3;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqqq' );
$u->save();

ok( $u );

my $fh = IO::File->new();
$fh->open( "< t/lib/User.pm" );

$u->file( { fh => $fh, filename => 'Module.pm' } );
$u->save();

$u->delete();

ok( !-f 't/uploads/user/' . $u->id . '/file/Module.pm' );

ok( !-e 't/uploads' );

$fh->close();
