package Metadata;

use strict;
use warnings;

use base 'Rose::DB::Object::Metadata';

__PACKAGE__->column_type_class(
    filename => 'Rose::DB::Object::Metadata::Column::Filename'
);

1;
