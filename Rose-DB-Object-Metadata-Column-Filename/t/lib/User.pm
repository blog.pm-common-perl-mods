package User;

use strict;

use base qw(DB::Object);

use Metadata;
sub meta_class { 'Metadata' }

__PACKAGE__->meta->setup(
   table => 'user',

   columns => [
       qw/ id name /,
       #file => { type => 'filename', directory => 't/uploads' }
       file => { type => 'filename' }
   ],

   primary_key_columns => [ qw/ id / ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
