#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 9;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqqq' );
$u->save();

my $u1 = User->new( name => 'qqqq1' );
$u1->save();

ok( $u );
ok( $u1 );

my $fh = IO::File->new();
$fh->open("< t/lib/User.pm");

$u->file({ fh => $fh, filename => 'Module.pm' });
$u->save();

is( $u->file, 'Module.pm' );
ok( -f 't/uploads/user/' . $u->id . '/file/Module.pm' );

$u1->file({ fh => $fh, filename => 'Module.pm' });
$u1->save();

is( $u1->file, 'Module.pm' );
ok( -f 't/uploads/user/' . $u1->id . '/file/Module.pm' );

$u->file(undef);
$u->save;

ok( !-e 't/uploads/user/' . $u->id );
ok( -e 't/uploads/user/' . $u1->id );

$u1->file(undef);
$u1->save;

ok( !-e 't/uploads' );

$u->delete();
$u1->delete();

$fh->close();
