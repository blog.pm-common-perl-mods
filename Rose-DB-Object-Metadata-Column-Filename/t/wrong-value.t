#!/usr/bin/perl

use strict;
use warnings;

use Test::More 'tests' => 3;
use IO::File;

use lib 't/lib';

use NewDB;
use User;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'qqqq' );
$u->save();

ok( $u );

$u->file('123');
$u->save();
ok( not defined $u->file );

$u->file({ fh => 'bu', filename => 'aaa' });
$u->save();
ok( not defined $u->file );

$u->delete();
