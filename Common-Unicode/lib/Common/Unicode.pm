package Common::Unicode;

BEGIN {
    $YAML::Syck::ImplicitUnicode = 1
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
