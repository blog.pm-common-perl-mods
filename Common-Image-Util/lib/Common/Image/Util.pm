package Common::Image::Util;
use Moose;

use Imager;
use Path::Class::File;

sub thumbnail {
    my ( $self ) = shift;
    my %args = @_;

    my $image = Imager->new;

    $image->read( file => $args{ image_fullpath } )
      or die $image->errstr;

    my ( $scaleX, $scaleY ) = ( $args{ thumb_geometry } =~ m/(\d+)?x(\d+)?/ );

    if ( $scaleX && $scaleY ) {
        $image = $image->scale(
            xpixels => $scaleX,
            ypixels => $scaleY,
        );

        $image = $image->crop(
            left   => ( $image->getwidth - $scaleX ) / 2,
            width  => $scaleX,
            top    => ( $image->getheight - $scaleY ) / 2,
            height => $scaleY
        ) or die $image->errstr;
    } elsif ( $scaleX ) {
        $image = $image->scale( xpixels => $scaleX );
    } else {
        $image = $image->scale( ypixels => $scaleY );
    }

    my $thumb_fullpath = $args{ thumb_fullpath };

    unless ( $thumb_fullpath->isa( 'Path::Class::File' ) ) {
        $thumb_fullpath = Path::Class::File->new( $thumb_fullpath );
    }

    $thumb_fullpath->dir->mkpath;

    $image->write( file => $thumb_fullpath->stringify )
      or die $image->errstr;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
