use strict;
use Test::More tests => 4;

use Common::Image::Util;

my $generator = Common::Image::Util->new();

ok( $generator );

$generator->thumbnail(
    image_fullpath => 't/static/linux.png',
    thumb_fullpath => 't/static/linux-thumb.png',
    thumb_geometry => '100x100'
);

ok( -f 't/static/linux-thumb.png' );
unlink 't/static/linux-thumb.png';

$generator->thumbnail(
    image_fullpath => 't/static/linux.png',
    thumb_fullpath => 't/static/linux-thumb.png',
    thumb_geometry => '100x'
);

ok( -f 't/static/linux-thumb.png' );
unlink 't/static/linux-thumb.png';

$generator->thumbnail(
    image_fullpath => 't/static/linux.png',
    thumb_fullpath => 't/static/linux-thumb.png',
    thumb_geometry => 'x100'
);

ok( -f 't/static/linux-thumb.png' );
unlink 't/static/linux-thumb.png';
