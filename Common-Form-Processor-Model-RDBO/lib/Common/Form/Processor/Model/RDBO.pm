package Common::Form::Processor::Model::RDBO;
use strict;
use warnings;
use base 'Form::Processor';

our $VERSION = '0.01';

sub validate {
    my $self = shift;

    my $ok = $self->SUPER::validate( @_ );
    return $ok unless $ok;

    $self->validate_model();
}

sub validate_model {
    my ( $self ) = @_;

    return unless $self->validate_unique;

    return 1;
}

sub validate_unique {
    my ( $self ) = @_;

    my $unique = $self->profile->{ unique } || return 1;

    my $item = $self->item;

    my $class = ref( $item ) || $self->object_class;

    my $found_error = 0;

    for my $field ( map { $self->field( $_ ) } @$unique ) {
        next if $field->errors;

        my $value = $field->value;
        next unless defined $value;

        my $name = $field->name;

        # unique means there can only be on in the database like it.
        my $match = $class->new( $name => $value );
        $match->load( speculative => 1 );
        next if $match->not_found;

        next if $self->items_same( $item, $match );

        $field->add_error( 'Value must be unique in the database' );
        $found_error++;
    }

    return !$found_error;
}

sub items_same {
    my ( $self, $item1, $item2 ) = @_;

    # returns true if both are undefined
    return 1 if not defined $item1 and not defined $item2;

    # return false if either undefined
    return unless defined $item1 and defined $item2;

    return $self->primary_key( $item1 ) eq $self->primary_key( $item2 );
}

sub primary_key {
    my ( $self, $item ) = @_;
    return join '|',
      $item->meta->table,
      map { $_ . '=' . ( $item->$_ || '.' ) } $item->meta->primary_key_columns;
}

sub update_from_form {
    my ( $self, $params ) = @_;

    return unless $self->validate( $params );

    my $item = $self->item;
    my $class = ref( $item ) || $self->object_class;

    my %fields = map { $_->name, $_ } grep { !$_->noupdate } $self->fields;

    $item = $class->new() unless $item;
    while ( my ( $name, $field ) = each %fields ) {
        if ( $item->can( $name ) ) {
            if ( defined $item->$name && defined $field->value ) {
                next if $item->$name eq $field->value;
            }
            $item->$name( $field->value );
        }
    }

    return $item;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 SEE ALSO

L<Form::Processor>

=cut

1;
