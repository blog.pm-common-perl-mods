use strict;
use Test::More tests => 3;

use Rose::DB::Object::Util 'has_modified_columns';

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $form = Form->new();

ok( $form->validate( { name => 'tom' } ) );

my $item = $form->update_from_form();
$item->save();

$form = Form->new( $item );
$form->validate( { name => 'tom' } );
$form->update_from_form( $item );
is( has_modified_columns( $item ), 0 );

$form = Form->new( $item );
$form->validate( { name => 'to' } );
$form->update_from_form();
ok( has_modified_columns( $item ) );
$item->save();

$item->delete();
