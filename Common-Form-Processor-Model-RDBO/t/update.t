use strict;
use Test::More tests => 2;

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $u = User->new( name => 'foo' );
$u->save();

my $form = Form->new( $u );
ok( $form->validate( { name => 'bar' } ) );

diag $form->field('name')->errors;

$form->update_from_form();

is( $u->name, 'bar' );

$u->delete();
