package NewDB;

use DBI;

sub new {
    my $class = shift;

    my $self = { db => "/tmp/form-processor-model-rdbo.db" };

    bless $self, $class;

    return $self;
}

sub init {
    my $self = shift;

    my $db = $self->{ db };

    unless ( -f $db ) {
        warn 'Creating new db...';

        my $dbh = DBI->connect( "dbi:SQLite:$db" ) or die $DBI::errstr;

        $dbh->do( <<SQL );
CREATE TABLE `user` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `name` CHARACTER VARYING(255) NOT NULL,
  UNIQUE (`name`)
);
SQL

        $dbh->disconnect();
    }
}

sub cleanup {
    my $self = shift;

    unlink $self->{ db };
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
