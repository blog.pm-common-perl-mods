package User;

use strict;

use base qw(DB::Object);

__PACKAGE__->meta->setup(
   table => 'user',

   columns => [
       qw/ id name  /
   ],

   primary_key_columns => [ qw/ id / ],

   unique_key => [ qw/ name / ],
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
