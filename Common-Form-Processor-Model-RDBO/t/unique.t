use strict;
use Test::More tests => 3;

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $form = Form->new();

ok( $form->validate( { name => 'haha' } ) );

my $u = User->new( name => 'haha' );
$u->save();

ok( not defined $form->validate( { name => 'haha' } ) );

$form = Form->new( $u );
ok( $form->validate( { name => 'haha' } ) );

$u->delete();
