use strict;
use Test::More tests => 2;

use lib 't/lib';

use NewDB;
use User;
use Form;

my $db = NewDB->new();

$db->init();

my $form = Form->new();

ok( $form->validate( { name => 'tom' } ) );

my $item = $form->update_from_form();
$item->save();

my $u = User->new( name => 'tom' );
$u->load( speculative => 1 );
is( $u->not_found, 0 );

$u->delete();
