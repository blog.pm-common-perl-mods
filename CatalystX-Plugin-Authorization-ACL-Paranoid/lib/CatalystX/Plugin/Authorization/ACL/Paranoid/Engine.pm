package CatalystX::Plugin::Authorization::ACL::Paranoid::Engine;

use strict;
use warnings;

sub new {
    my ( $class, $c ) = @_;

    my $self = bless { rules => {} }, $class;

    return $self;
}

sub rules {
    my ( $self, $rules ) = @_;

    $self->{ rules } = {};

    while ( my ( $action, $check ) = each %$rules ) {
        $action =~ s/^\///o unless $action eq '/';

        $self->{ rules }->{ $action } =
          ref $check eq 'CODE' ? $check : sub { $check }
    }
}

sub is_allowed {
    my ( $self, $c, $class, $action ) = @_;

    # skip internal links
    if (
        $action
        && (   $action =~ m/\/?(?:_[A-Z]+|auto|begin|end)$/o
            || $action =~ m/(?:->process$|^default$)/o )
      )
    {
        return 1;
    }

    if ( my $check = $self->{ rules }->{ $action } ) {
        my $ret = $check->( $c );

        return $ret;
    }

    if ( my $check = $self->{ rules }->{ '*' } ) {
        return $check->( $c );
    }

    return 0;
}

sub detach {
    my ( $self ) = @_;

    $self->{ default } = $_[1] if @_;

    return $self->{ default } || '/end';
}

=head1 AUTHOR

vti

=head1 COPYRIGHT

This program is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
