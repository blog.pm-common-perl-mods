package CatalystX::Plugin::Authorization::ACL::Paranoid;
use base qw/Class::Data::Inheritable/;

# based upon Catalyst::Plugin::Authorization::ACL;

use strict;
use warnings;

#use NEXT;
use CatalystX::Plugin::Authorization::ACL::Paranoid::Engine;

use Scalar::Util();

BEGIN { __PACKAGE__->mk_classdata( '_acl_engine' ) }

sub setup_actions {
    my $c = shift;
    my $ret = $c->NEXT::setup_actions( @_ );

    $c->_acl_engine(
        CatalystX::Plugin::Authorization::ACL::Paranoid::Engine->new( $c ) );

    return $ret;
}

sub execute {
    my ( $c, $class, $action ) = @_;

    local $NEXT::NEXT{ $c, "execute" };

    if ( Scalar::Util::blessed( $action ) ) {
        unless ( $c->_acl_engine->is_allowed( $c, $class, $action ) ) {
            $c->log->debug( 'Access denied to ' . $action );
            $c->res->status( 403 );
            $c->error( 'Access denied' );
            $c->detach( $c->_acl_engine->detach );
            return;
        } else {
            $c->log->debug( 'Access granted to ' . $action );
        }
    }

    $c->NEXT::execute( $class, $action );
}

sub acl {
    my ( $c ) = shift;
    my %args = @_;

    #use Data::Dumper;

    $c->_acl_engine->detach( $args{detach} );

    $c->_acl_engine->rules( $args{rules} );
}

=head1 AUTHOR

vti

=head1 COPYRIGHT

This program is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
