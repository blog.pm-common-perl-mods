package TestApp;

use strict;
use warnings;

use Catalyst qw/
  Session
  Session::Store::Dummy
  Session::State::Cookie

  Authentication

  +CatalystX::Plugin::Authorization::ACL::Paranoid
  /;

sub index : Local {
}

sub user_login : Local {
    my ( $self, $c ) = @_;

    return unless $c->req->param('user') && $c->req->param('password');
    unless ( $c->authenticate( {
                username => $c->req->param( 'user' ),
                password => $c->req->param( 'password' )
            }))
    {
        $c->forward('/forbidden');
    }
}

sub user_logout : Local {
    my ( $self, $c ) = @_;

    $c->logout();
}

sub post_list : Local {
}

sub post_add : Local {
}

sub post_delete : Local {
}

sub forbidden : Private {
    my ( $self, $c ) = @_;

    $c->res->status( '403' );
    $c->error( 'Access denied' );
}

sub end : Private {
    my ( $self, $c ) = @_;

    $c->error( 0 );
}

__PACKAGE__->config(
    'authentication' => {
        'realms' => {
            'admins' => {
                'store' => {
                    'users' => { 'user' => { 'password' => 'password' } },
                    'class' => 'Minimal'
                },
                'credential' => {
                    'password_type'  => 'clear',
                    'password_field' => 'password',
                    'class'          => 'Password'
                }
            }
        },
        'default_realm' => 'admins'
    }
);

__PACKAGE__->setup;

__PACKAGE__->acl(
    rules => {
        '/index' => 1,

        '/user_login'  => sub { !shift->user_exists() },
        '/user_logout' => sub { shift->user_exists() },

        '/post_list' => 1,
        '/post_add'  => sub { shift->user_exists() },

        '*' => sub {
            my $c = shift;
            $c->user_exists() && $c->user_in_realm( 'admins' );
          }
    }
);

1;
