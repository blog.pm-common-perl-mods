#!/usr/bin/perl

use strict;
use warnings;

use lib 't/lib';

use Test::More 'no_plan';

BEGIN {
    eval {
        require Test::WWW::Mechanize::Catalyst;
        require Catalyst::Plugin::Authentication;
        require Catalyst::Plugin::Session;
        require Catalyst::Plugin::Session::State::Cookie;
    };

    plan skip_all => 'Many plugins are required' if $@;

    use_ok 'Test::WWW::Mechanize::Catalyst', 'TestApp';
}

my $m = Test::WWW::Mechanize::Catalyst->new;

is_allowed( '/', 'index page' );

is_allowed( '/user_login', 'index page' );

is_denied( '/post_delete', 'post delete' );

is_denied( '/user_logout', 'logout before login' );

login( 'user', 'password');

is_denied( '/user_login', 'login again after login' );

is_allowed( '/post_delete', 'delete post after login');

is_allowed( '/user_logout', 'logout after login' );

sub login {
    my ( $l, $p ) = @_;
    is_allowed( "/user_login?user=$l&password=$p" );
}

sub is_denied {
    my $path = shift;
    my $res = $m->get( $path );
    is( $res->status_line, '403 Forbidden' );
}

sub is_allowed {
    my ( $path, $text ) = @_;
    $m->get_ok( $path );
}

