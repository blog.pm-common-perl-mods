package Rose::DB::UTF8Columns;

use strict;

sub utf8_columns {
    my ( $self ) = shift;

    return if ref $self;

    my $meta = Rose::DB::Object::Metadata->for_class( $self );

    foreach my $column_name ( @_ ) {
        my $column = $meta->column( $column_name );

        $column->add_trigger(
            event => 'on_get',
            code  => sub {
                my $self  = shift;
                my $value = $self->$column_name;

                utf8::decode( $value ) unless utf8::is_utf8( $value );

                $self->$column_name( $value );

            }
        );
    }
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
