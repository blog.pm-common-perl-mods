use inc::Module::Install;

name 'Rose-DB-UTF8Columns';

all_from 'lib/Rose/DB/UTF8Columns.pm';
perl_version '5.006';

requires(
    'Rose::DB' => '',

);

auto_install;

WriteAll;
