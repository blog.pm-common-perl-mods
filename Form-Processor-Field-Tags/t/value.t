use strict;
use Test::More tests => 5;

use Form::Processor::Field::Tags;

my $field = Form::Processor::Field::Tags->new(
    name => 'test_field',
    type => 'fake',
    form => undef,
);

ok( defined $field, 'new() called' );

$field->input( 'one' );
$field->validate_field;
ok( !$field->has_error );
is_deeply( $field->value(), { name => 'one' } );

$field->input( 'one, two' );
$field->validate_field;
ok( !$field->has_error );
is_deeply( $field->value(), [ { name => 'one' }, { name => 'two' } ] );

diag $field->format_value;
