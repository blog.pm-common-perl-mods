use strict;
use Test::More tests => 9;

use Form::Processor::Field::Tags;

my $field = Form::Processor::Field::Tags->new(
    name => 'test_field',
    type => 'fake',
    form => undef,
);

ok( defined $field, 'new() called' );

$field->input( 'one' );
$field->validate_field;
ok( !$field->has_error );
is( $field->value, 'one' );

$field->input( '     one       ' );
$field->validate_field;
ok( !$field->has_error );
is( $field->value, 'one' );

$field->input( ',,,     one       ' );
$field->validate_field;
ok( !$field->has_error );
is( $field->value, 'one' );

$field->input( '     one  ,     ' );
$field->validate_field;
ok( !$field->has_error );
is( $field->value, 'one' );
