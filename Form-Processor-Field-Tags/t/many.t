use strict;
use Test::More tests => 13;

use Form::Processor::Field::Tags;

my $field = Form::Processor::Field::Tags->new(
    name => 'test_field',
    type => 'fake',
    form => undef,
);

ok( defined $field, 'new() called' );

$field->input( 'one, two, three' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );

$field->input( ',one, two, three' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );

$field->input( 'one, two, three,' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );

$field->input( ',,,one, two , three,' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );

$field->input( 'one, two, , three,' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );

$field->input( '     one      , two       ,  three      ' );
$field->validate_field;
ok( !$field->has_error );

is_deeply( $field->value, [ qw/one two three/ ] );
