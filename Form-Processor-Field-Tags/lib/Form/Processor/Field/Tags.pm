package Form::Processor::Field::Tags;
use strict;
use warnings;
use base 'Form::Processor::Field::Multiple';
our $VERSION = '0.01';

sub input_to_value {
    my $self = shift;

    my $v = $self->input;

    return unless defined $v;

    $v =~ s/,{2,}/,/g;
    $v =~ s/^,//g;
    $v =~ s/,$//g;
    $v =~ s/\s*,\s*/,/g;

    my @tags = ();
    foreach my $tag (split(',', $v)) {
        $tag =~ s/^\s*//;
        $tag =~ s/\s*$//;
        push @tags, $tag if defined $tag && $tag ne '';
    }

    if ( @tags > 1 ) {
        my $values = [ map { { name => $_ } } @tags ];
        $self->value( $values );
    } else {
        $self->value( { name => $tags[ 0 ] } );
    }

    return;
}

sub validate_field {
    my $field = shift;

    $field->reset_errors;
    $field->value(undef);


    # See if anything was submitted
    unless ( $field->any_input ) {
        $field->add_error( $field->required_message )
            if $field->required;

        return !$field->required;
    }

    #return unless $field->test_multiple;
    #return unless $field->test_options;
    return unless $field->validate;
    return unless $field->test_ranges;


    # Now move data from input -> value
    $field->input_to_value;

    return $field->validate_value unless $field->has_error;

    return;
}

sub validate {
    my $self = shift;

    return 1;
}

=head1 AUTHORS

vti

=head1 COPYRIGHT

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
