package Common::Validator;
use Moose;

use Common::Validator::Field;

has 'fields' => (
    is      => 'ro',
    isa     => 'HashRef[Common::Validator::Field]',
    default => sub { {} }
);

has 'params' => ( is => 'ro', isa => 'HashRef[Str]', required => 1 );

has 'has_errors' => ( is => 'rw', isa => 'Bool', default => 0 );

sub BUILD {
    my ( $self ) = @_;

    $self->init_fields();
}

sub init_fields { }

sub add_fields {
    my ( $self ) = shift;
    my %fields = @_;

    foreach my $key ( keys %fields ) {
        my $options = $fields{ $key };

        my $field = Common::Validator::Field->new(
            name     => $key,
            required => delete $options->{ required },
            value => $self->params->{ $key }
        );

        foreach my $c ( keys %$options ) {
            $field->add_constraint(
                $self->load( "Common::Validator::Constraint::" . ucfirst $c )
                  ->new( args => $options->{ $c } ) );
        }

        $self->fields->{ $field->name } = $field;
    }
}

sub load {
    my ( $self, $class ) = @_;

    my $error;
    {
        local $@;
        my $file = $class . '.pm';
        $file =~ s{::}{/}g;
        eval { CORE::require($file) };
        $error = $@;
    }

    die $error if $error;

    return $class;
}

sub error {
    my ( $self, $name ) = @_;

    return $self->fields->{ $name }->error;
}

sub errors {
    my ( $self ) = @_;

    my $errors = {};

    foreach my $field ( values %{ $self->fields } ) {
        $errors->{ $field->name } = $field->error;
    }

    return $errors;
}

sub field {
    my ( $self, $name ) = @_;
    
    return $self->fields->{ $name };
}

sub value {
    my ( $self, $name ) = @_;

    return $self->fields->{ $name }->value;
}

sub clear_errors {
    my ( $self ) = @_;

    foreach my $field ( values %{ $self->fields } ) {
        $field->error('');
    }
}

sub is_valid {
    my ( $self ) = shift;

    $self->clear_errors();

    foreach my $field ( values %{ $self->fields } ) {
        if ( $field->required && $field->is_empty() ) {
            $field->error( 'Required' );
            $self->has_errors( 1 );
            next;
        }

        unless ( $field->is_valid() ) {
            $self->has_errors( 1 );
        }
    }

    return !$self->has_errors;
}

sub values {
    my $self = shift;

    my $values = {};

    foreach my $field ( values %{ $self->fields } ) {
        $values->{ $field->name } = $field->value;
    }

    return $values;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
