package Common::Validator::Field;
use Moose;

has 'name' => ( is => 'rw', isa => 'Str', required => 1 );

has 'required' => ( is => 'rw', isa => 'Bool', default => 0 );

has 'constraints' => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );

has 'error' => ( is => 'rw', isa => 'Str' );

has 'value' => ( is => 'rw', isa => 'Maybe[Str]' );

sub add_constraint {
    my ( $self, $constraint ) = @_;

    push @{ $self->{ constraints } }, $constraint;
}

sub is_valid {
    my ( $self ) = @_;

    foreach my $c ( @{ $self->constraints } ) {
        unless ( $c->is_valid( $self->value ) ) {
            $self->error( $c->errorstr );
            return 0;
        }
    }

    return 1;
}

sub is_defined {
    my ( $self ) = @_;

    return defined $self->value ? 1 : 0;
}

sub is_empty {
    my ( $self ) = @_;

    return 1 unless $self->is_defined();

    return $self->value eq '' ? 1 : 0;
}

1;
