package Common::Validator::Constraint;
use Moose;

has 'errorstr' => ( is => 'ro', isa => 'Str', default => 'Invalid input' );

sub is_valid { 0 }

__PACKAGE__->meta->make_immutable;

1;
