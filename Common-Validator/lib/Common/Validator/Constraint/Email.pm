package Common::Validator::Constraint::Email;
use Moose;

use Email::Valid;

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'Bool', required => 1 );

sub is_valid {
    my ( $self, $value ) = @_;

    return Email::Valid->address( $value ) ? 1 : 0;
}

__PACKAGE__->meta->make_immutable;

1;
