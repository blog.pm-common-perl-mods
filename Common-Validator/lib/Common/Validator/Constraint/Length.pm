package Common::Validator::Constraint::Length;
use Moose;

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'ArrayRef', required => 1 );

sub is_valid {
    my ( $self, $value ) = @_;

    my $len = length $value;

    my ( $min, $max ) = @{ $self->args };

    return $len >= $min && $len <= $max ? 1 : 0;
}

__PACKAGE__->meta->make_immutable;

1;
