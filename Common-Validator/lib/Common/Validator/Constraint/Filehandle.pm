package Common::Validator::Constraint::Filehandle;
use Moose;

use File::MimeInfo::Magic;

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'HashRef', required => 1 );

sub is_valid {
    my ( $self, $fh ) = @_;

    return 0 unless -f $fh;

    if ( my $size = $self->args->{ size } ) {
        return 0 if ( stat $fh )[ 7 ] > $size;
    }

    if ( my $mimetype = $self->args->{ mimetype } ) {
        return 0 unless mimetype( $fh ) =~ $mimetype;
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

1;
