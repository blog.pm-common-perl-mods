package Common::Validator::Constraint::Uri;
use Moose;

use Data::Validate::URI 'is_uri';

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'Bool', required => 1 );

sub is_valid {
    my ( $self, $value ) = @_;

    return is_uri( $value ) ? 1 : 0;
}

__PACKAGE__->meta->make_immutable;

1;
