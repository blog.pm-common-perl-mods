package Common::Validator::Constraint::In;
use Moose;

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'ArrayRef', required => 1 );

sub is_valid {
    my ( $self, $value ) = @_;

    return grep { $value eq $_ } @{ $self->args };
}

__PACKAGE__->meta->make_immutable;

1;
