package Common::Validator::Constraint::Regexp;
use Moose;

extends 'Common::Validator::Constraint';

has 'args' => ( is => 'ro', isa => 'RegexpRef', required => 1 );

sub is_valid {
    my ( $self, $value ) = @_;

    return $value =~ $self->args ? 1 : 0;
}

__PACKAGE__->meta->make_immutable;

1;
