use strict;
use Test::More tests => 15;

use Common::Validator::Field;
use Common::Validator::Constraint::Length;

my $field = Common::Validator::Field->new( name => 'username', required => 1 );

ok( $field );

is( $field->name,     'username' );
is( $field->required, 1 );
is_deeply( $field->constraints, [] );

$field->add_constraint(
    Common::Validator::Constraint::Length->new( args => [ 3, 14 ] )
);

ok( not defined $field->value );

$field->value( 'ab' );

is( $field->is_defined(), 1 );
is( $field->is_empty(),   0 );

is( $field->value(), 'ab' );

ok( not defined $field->error );

is( $field->is_valid(), 0 );

ok( $field->error );

$field->value( undef );

is( $field->is_defined(), 0 );
is( $field->is_empty(),   1 );

$field->value( '' );
is( $field->is_defined(), 1 );
is( $field->is_empty(),   1 );

#is( $field->error, Common::Validator::Constraint::Length->errorstr );
