use strict;
use Test::More tests => 5;

package My::Validator;
use Moose;

extends 'Common::Validator';

sub init_fields {
    my ( $self ) = @_;

    $self->add_fields( username => { required => 1, length => [ 3, 8 ] } );
}

1;

package main;

my $validator = My::Validator->new(
    params => {
        a        => 'b',
        c        => 'd',
        username => 'hello'
    }
);

ok( $validator );

is( keys %{ $validator->fields }, 1 );

is( $validator->is_valid(), 1 );

is( $validator->value( 'username' ), 'hello' );

is_deeply( $validator->values(), { username => 'hello' } );

