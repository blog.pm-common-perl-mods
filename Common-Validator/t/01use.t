use strict;
use Test::More tests => 2;

BEGIN {
    use_ok( 'Common::Validator' );
    use_ok( 'Common::Validator::Field' );
}
