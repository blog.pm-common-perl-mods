use strict;
use Test::More tests => 4;

use Common::Validator::Constraint::Uri;

my $constraint = Common::Validator::Constraint::Uri->new( args => 1 );

ok( $constraint );

is( $constraint->is_valid( 'Hello' ), 0 );

is( $constraint->is_valid( 'http://example.com' ), 1 );

is( $constraint->is_valid( 'http://example.com http://example.com' ), 0 );
