use strict;
use Test::More tests => 2;

use Common::Validator::Constraint;

my $constraint = Common::Validator::Constraint->new();

ok( $constraint );

is( $constraint->is_valid(), 0 );
