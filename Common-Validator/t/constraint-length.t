use strict;
use Test::More tests => 4;

use Common::Validator::Constraint::Length;

my $constraint = Common::Validator::Constraint::Length->new( args => [ 3, 5 ] );

ok( $constraint );

is( $constraint->is_valid( 'Hello' ), 1 );

is( $constraint->is_valid( 'He' ), 0 );

is( $constraint->is_valid( 'Hello!' ), 0 );
