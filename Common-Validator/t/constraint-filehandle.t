use strict;
use Test::More tests => 3;
use IO::File;

use Common::Validator::Constraint::Filehandle;

my $constraint = Common::Validator::Constraint::Filehandle->new(
    args => {
        size     => 5669,
        mimetype => 'image/png'
    }
);

ok( $constraint );

my $fh = IO::File->new();

$fh->open("< $0");

is( $constraint->is_valid( $fh ), 0 );

$fh->close();

$fh->open("< t/extra/linux.png");

is( $constraint->is_valid( $fh ), 1 );

$fh->close();
