use strict;
use Test::More tests => 4;

use Common::Validator::Constraint::Email;

my $constraint = Common::Validator::Constraint::Email->new( args => 1 );

ok( $constraint );

is( $constraint->is_valid( 'Hello' ), 0 );

is( $constraint->is_valid( 'root@localhost.com' ), 1 );

is( $constraint->is_valid( 'root@localhost.com root@localhost.com' ), 0 );
