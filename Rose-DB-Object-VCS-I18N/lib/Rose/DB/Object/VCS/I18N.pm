package Rose::DB::Object::VCS::I18N;

use strict;

use base 'Rose::DB::Object::VCS';

sub commit {
    my $self   = shift;
    my %params = @_;

    return $self->SUPER::commit( @_ ) if $self->meta->i18n_static_rel_name;

    unless ( Rose::DB::Object::VCS::is_in_db( $self ) ) {
        $self->addtime( time ) unless $self->addtime;
        $self->revision( 1 );
        $self->i18n->user_id( $self->user_id );
        $self->save( noi18n => 1 );
        return;
    }

    $self->i18n->user_id( $self->user_id );
    $self->i18n->commit();

    return unless Rose::DB::Object::VCS::has_modified_columns( $self );

    my $diff = Rose::DB::Object::VCS::diff( $self );

    return unless defined $diff;

    $self->add_diffs( $diff );

    $self->addtime( time ) unless $self->addtime;
    $self->revision( $self->revision + 1 );
    $self->save();
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
