use Test::More tests => 12;
use strict;

use lib 't/lib';

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my $post = Post->new(
    orig_lang => 'en',
    name      => 'vti',
    content   => 'Hello',
    user_id   => 1
);
$post->commit();

$post = Post->new( id => $post->id );
$post->load( i18n => 'en' );

is( $post->name,     'vti' );
is( $post->revision, 1 );

is( $post->i18n->content,  'Hello' );
is( $post->i18n->revision, 1 );

$post->name( 'vti2' );
$post->commit();

is( $post->name,     'vti2' );
is( $post->revision, 2 );

is( $post->i18n->content,  'Hello' );
is( $post->i18n->revision, 1 );

$post->i18n->content( 'booo' );
$post->commit();

is( $post->name,     'vti2' );
is( $post->revision, 2 );

is( $post->i18n->content,  'booo' );
is( $post->i18n->revision, 2 );

1;
