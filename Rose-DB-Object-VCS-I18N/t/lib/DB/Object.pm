package DB::Object;

use strict;

use base qw/ DB::ObjectInit Rose::DB::Object / ;

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
