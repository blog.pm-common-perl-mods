package Metadata;

use strict;
use warnings;

use base qw/ Rose::DBx::Object::I18N::Metadata Rose::DB::Object::VCS::Metadata /;

#use Rose::Class::MakeMethods::Generic(
    #scalar => [ qw/ versioned_columns / ] );

1;
