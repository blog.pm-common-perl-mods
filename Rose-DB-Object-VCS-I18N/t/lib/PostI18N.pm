package PostI18N;

use strict;

use base qw/ DB::Object::I18N /;

use Rose::DB::Object::VCS ':all';

use Metadata;
sub meta_class { 'Metadata' }

__PACKAGE__->meta->setup(
    table => 'post_i18n',

    columns => [
        qw/
          i18nid
          user_id
          post_id
          lang
          istran
          content
          /,
          addtime => { type => 'epoch', default => 'now' },
          revision => { default => 1 }
    ],

    primary_key_columns => [ 'i18nid' ],

    relationships => [
        post => {
            class       => 'Post',
            key_columns => { post_id => 'id' },
            type        => 'many to one',
        },
        diffs => {
            type       => 'one to many',
            class      => 'PostI18NDiff',
            column_map => { i18nid => 'i18n_id' }
        },
    ],

    i18n_static_rel_name => 'post',

    versioned_columns => [ qw/ content / ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
