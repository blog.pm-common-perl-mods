package PostI18NDiff;

use strict;

use base qw/ DB::Object /;

__PACKAGE__->meta->setup(
    table => 'post_i18n_diffs',

    columns => [
        qw/ id user_id addtime i18n_id content revision /,
    ],

    primary_key_columns => [ qw/ id / ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
