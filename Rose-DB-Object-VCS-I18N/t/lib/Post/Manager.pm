package Post::Manager;

use strict;

use base 'Rose::DB::Object::I18N::Manager';

sub object_class { 'Post' }

__PACKAGE__->make_manager_methods( 'posts' );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
