package Post;

use strict;

use base qw(DB::Object::I18N);

use Rose::DB::Object::VCS::I18N ':all';

use Metadata;
sub meta_class { 'Metadata' }

__PACKAGE__->meta->setup(
    table => 'post',

    columns => [
        qw/ id user_id name orig_lang addtime revision /,
    ],

    primary_key_columns => [ qw/ id / ],

    unique_key => [ qw/ name / ],

    relationships => [
        post_i18n => {
            type       => 'one to many',
            class      => 'PostI18N',
            column_map => { id => 'post_id' }
        },
        diffs => {
            type       => 'one to many',
            class      => 'PostDiff',
            column_map => { id => 'post_id' }
        },
    ],

    i18n_translation_rel_name => 'post_i18n',

    versioned_columns => [ qw/ name / ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
