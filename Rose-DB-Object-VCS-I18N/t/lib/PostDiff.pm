package PostDiff;

use strict;

use base qw/ DB::Object /;

__PACKAGE__->meta->setup(
    table => 'post_diffs',

    columns => [
        qw/ id user_id addtime post_id name revision /
    ],

    primary_key_columns => [ qw/ id / ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
