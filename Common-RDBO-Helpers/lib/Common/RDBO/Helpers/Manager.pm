package Common::RDBO::Helpers::Manager;

use strict;
use warnings;

use Rose::Object::MixIn();
our @ISA = qw(Rose::Object::MixIn);

use Hash::Merge 'merge';

__PACKAGE__->export_tag(
    all => [
        qw/ get_objects_by_related get_similar_objects
          get_objects_archive get_objects_archive_month_list
          get_objects_pager get_objects_cloud set_related_objects_column
          /
    ]
);

sub get_objects_by_related {
    my ( $self ) = shift;
    my %args = @_;

    my $rel_name = delete $args{ rel_name };
    warn 'no rel_name' and return unless $rel_name;

    my ( $rel ) =
      grep { $_->name eq $rel_name } $self->object_class->meta->relationships;
    warn 'rel_name is incorrect' and return unless $rel;

    my @rel_objects =
      ref $args{ rel_objects }
      ? @{ delete $args{ rel_objects } }
      : ( delete $args{ rel_objects } );
    warn 'no rel_objects' and return unless scalar @rel_objects;

    my ( $rel_pk_name ) = $rel->foreign_class->meta->primary_key_column_names;

    my $rel_pks = [ map { ref $_ ? $_->$rel_pk_name : $_ } @rel_objects ];

    return unless $rel_pks->[ 0 ];

    my ( $pk ) = $self->object_class->meta->primary_key_column_names;
    return unless $pk;

    my $cond = merge {
        query        => [ "$rel_name.$rel_pk_name" => $rel_pks ],
        with_objects => [ $rel_name ],
        group_by     => "t1.$pk"
      },
      \%args;

    return $self->get_objects( %$cond );
}

sub get_similar_objects {
    my ( $self ) = shift;
    my %args = @_;

    my $object = delete $args{ object };
    warn 'no object' and return unless $object;

    my $rel_name = $args{ rel_name };
    warn 'no rel_name' and return unless $rel_name;

    my ( $pk ) = $self->object_class->meta->primary_key_column_names;
    return unless $pk;

    my $cond = merge {
        rel_objects => [ $object->$rel_name ],
        query       => [ "t1.$pk" => { ne => $object->$pk } ]
      },
      \%args;

    return $self->get_objects_by_related( %$cond );
}

sub get_objects_archive {
    my $self = shift;
    my %args = @_;

    my $column = delete $args{ column };
    warn 'no column' and return unless $column;

    warn 'column does not exist' and return
      unless grep { $_ eq $column }
          @{ $self->object_class->meta->column_names };

    my $year  = delete $args{ year };
    my $month = delete $args{ month };
    my $day   = delete $args{ day };

    my ( $Dy, $Dm, $Dd ) = ( 0, 0, 0 );
    if ( $day ) {
        $Dd = 1;
    } elsif ( $month ) {
        $Dm = 1;
    } else {
        $Dy = 1;
    }

    my $from = eval {
        DateTime->new(
            year  => $year,
            month => $month || 1,
            day   => $day || 1
        );
    };
    return if $@;

    my $to = $from->clone->add( years => $Dy, months => $Dm, days => $Dd );

    my $cond = merge {
        query => [
            $column => { ge => $from->epoch },
            $column => { lt => $to->epoch }
        ]
      },
      \%args;

    return $self->get_objects( %$cond );
}

sub get_objects_archive_month_list {
    my ( $self ) = shift;
    my %args = @_;

    my $column = delete $args{ column };
    warn 'no column' and return unless $column;

    warn 'column does not exist' and return
      unless grep { $_ eq $column }
          @{ $self->object_class->meta->column_names };

    my $oldest = $self->get_objects(
        select  => $column,
        sort_by => "$column ASC",
        limit   => 1
    );

    my $newest = $self->get_objects(
        select  => $column,
        sort_by => "$column DESC",
        limit   => 1
    );

    $oldest = $oldest->[ 0 ];
    $newest = $newest->[ 0 ];

    return unless $oldest && $newest;

    $oldest = $oldest->$column;
    $newest = $newest->$column;

    if ( $oldest == $newest ) {
        return [
            {
                count      => 1,
                year       => $newest->year,
                month      => $newest->month,
                month_name => $newest->month_name
            }
        ];
    }

    my $list = [];
    my $dt   = $newest;
    while ( $dt > $oldest ) {
        my $count = $self->get_objects_count(
            query => [
                $column => { gt => $dt->clone->subtract( months => 1 )->epoch },
                $column => { le => $dt->epoch }
            ]
        );

        push @$list,
          {
            count      => $count,
            year       => $dt->year,
            month      => $dt->month,
            month_name => $dt->month_name
          }
          if $count;

        $dt->subtract( months => 1 );
    }

    return $list;
}

sub get_objects_pager {
    my ( $self ) = shift;
    my %args = @_;

    my $page = delete $args{ page };
    my $per_page =
         delete $args{ per_page }
      || delete $args{ page_size }
      || $self->default_objects_per_page,

      my $total = $self->get_objects_count( %args );

    $page = 1
      unless $page && $page =~ m/^\d+$/o && $per_page * ( $page - 1 ) < $total;

    return {
        total    => $total,
        page     => $page,
        per_page => $per_page
    };
}

sub get_objects_cloud {
    my $self = shift;
    my %args = @_;

    my $rel_name = delete $args{ rel_name };
    warn 'no rel_name' and return unless $rel_name;

    my ( $rel ) =
      grep { $_->name eq $rel_name } $self->object_class->meta->relationships;
    warn 'rel_name is incorrect' and return unless $rel;

    my $count_method = delete $args{ count_method };
    warn 'no count_method' and return unless $count_method;

    my $active_column = delete $args{ active_column };
    warn 'no rel_active_column' and return unless $active_column;

    my ( $rel_pk_name ) = $rel->foreign_class->meta->primary_key_column_names;
    my ( $pk_name )     = $self->object_class->meta->primary_key_column_names;

    my $cond = merge {
        select => [
            $pk_name, $active_column,
            \"COUNT(t2.$rel_pk_name) AS $count_method"
          ],
          sort_by      => "$active_column ASC",
          group_by     => $pk_name,
          with_objects => $rel_name
    }, \%args;

    my $objects = $self->get_objects( %$cond );

    return unless @$objects;

    my $empty_objects = [];

    my $ar = [];
    foreach my $object ( @$objects ) {
        my $hash = {};
        foreach my $method ( 'id', $active_column ) {
            $hash->{ $method } = $object->$method;
        }
        $hash->{ count } = $object->$count_method;
        if ( $hash->{ count } > 0 ) {
            push @$ar, $hash;
        } else {
            push @{ $empty_objects }, $hash;
        }
    }

    return unless @$ar;

    # from HTML::TagCloud
    my @sorted_by_count = sort { $b->{ count } <=> $a->{ count } } @$ar;

    my $min = log( $sorted_by_count[ -1 ]->{ count } );
    my $max = log( $sorted_by_count[ 0 ]->{ count } );
    my $factor;

    my $levels = 24;

    # special case all objects having the same count
    if ( $max - $min == 0 ) {
        $min    = $min - $levels;
        $factor = 1;
    } else {
        $factor = $levels / ( $max - $min );
    }

    if ( scalar @$ar < $levels ) {
        $factor *= ( scalar @$ar / $levels );
    }

    foreach my $object ( @$ar ) {
        $object->{ level } =
          int( ( log( $object->{ count } ) - $min ) * $factor );
    }

    return $ar;
}

sub set_related_objects_column {
    my ( $self ) = shift;
    my %args = @_;

    my @objects =
      ref $args{ objects }
      ? @{ delete $args{ objects } }
      : ( delete $args{ objects } );
    warn 'no objects' and return unless scalar @objects;

    my $objects_rel_name = delete $args{ objects_rel_name };
    warn 'no objects_rel_name' and return unless $objects_rel_name;

    my ( $objects_rel ) =
      grep { $_->name eq $objects_rel_name } $objects[ 0 ]->meta->relationships;
    warn 'objects_rel_name is incorrect' and return unless $objects_rel;

    my $objects_active_column = delete $args{ objects_active_column };
    warn "objects_active_column '$objects_active_column' does not exist"
      and return
      unless grep { $_ eq $objects_active_column }
          @{ $objects[ 0 ]->meta->column_names };

    my $active_column = delete $args{ active_column };
    warn 'active_column does not exist'
      and return
      unless $active_column
          and grep { $_ eq $active_column }
          @{ $self->object_class->meta->column_names };

    my ( $objects_pk, $pk ) = %{ $objects_rel->column_map };

    my @query_args = @{ $objects_rel->query_args };

    my $cond = merge {
        query => [ $pk => [ map { $_->$objects_pk } @objects ], @query_args ],
        select => [ $objects_pk, $pk, $active_column ],
      },
      \%args;

    my $items = $self->get_objects( %$cond );

    my %items = map { $_->$pk => $_ } @$items;

    foreach ( @objects ) {
        $_->$objects_active_column( $items{ $_->$objects_pk } )
          if $items{ $_->$objects_pk };
    }
}

#sub set_related_objects_with_count {
#    my ( $self ) = shift;
#    my %args = @_;

#    my @objects =
#      ref $args{ objects }
#      ? @{ delete $args{ objects } }
#      : ( delete $args{ objects } );
#    warn 'no objects' and return unless scalar @objects;

#    my $objects_rel_name = delete $args{ objects_rel_name };
#    warn 'no objects_rel_name' and return unless $objects_rel_name;

#    my ( $objects_rel ) = grep { $_->name eq $objects_rel_name }
#      $objects[0]->meta->relationships;
#    warn 'objects_rel_name is incorrect' and return unless $objects_rel;

#    my $objects_count_column = delete $args{ objects_count_column };
#    warn 'objects_count_column does not exist' and return
#      unless grep { $_ eq $objects_count_column } @{ $objects[0]->meta->column_names };

#    my $count_column = delete $args{ count_column };
#    warn 'count_column does not exist' and return
#      unless grep { $_ eq $count_column } @{ $self->meta->column_names };

#    my $cond = merge {
#        query => [ master_id => [ map { $_->id } @objects ] ],
#        select   => [ 'master_id', \"COUNT(id) AS $count_column" ],
#        group_by => 'master_id'
#      },
#      \%args;

#    my $items = $self->get_objects( %$cond );

#    my %items = map { $_->master_id => $_->$count_column } @$items;

#    foreach ( @objects ) {
#        $_->$objects_count_column( $items{ $_->id } )
#          if $items{ $_->id };
#    }
#}

1;
