package Common::RDBO::Helpers;

use strict;
use warnings;

use Rose::Object::MixIn();
our @ISA = qw(Rose::Object::MixIn);

use Hash::Merge 'merge';

__PACKAGE__->export_tag(
    all => [ qw/ get_prev_objects get_next_objects get_master_object / ] );

sub get_prev_objects {
    my ( $self ) = shift;
    my %args = @_;

    my $manager = delete $args{ manager } || $self->meta->class . '::Manager';

    my $column = delete $args{ column };
    warn 'no column' and return unless $column;

    warn 'column does not exist' and return
      unless grep { $_ eq $column } @{ $self->meta->column_names };

    my $cond = merge {
        query   => [ $column => { lt => $self->$column } ],
        sort_by => "$column DESC",
      },
      \%args;

    return $manager->get_objects( object_class => $self->meta->class, %$cond );
}

sub get_next_objects {
    my ( $self ) = shift;
    my %args = @_;

    my $manager = delete $args{ manager } || $self->meta->class . '::Manager';

    my $column = delete $args{ column };
    warn 'no column' and return unless $column;

    warn 'column does not exist' and return
      unless grep { $_ eq $column } @{ $self->meta->column_names };

    my $cond = merge {
        query   => [ $column => { gt => $self->$column } ],
        sort_by => "$column ASC",
      },
      \%args;

    return $manager->get_objects( object_class => $self->meta->class, %$cond );
}

sub get_master_object {
    my $self = shift;

    if ( $self->can( 'type' ) ) {
        my $type = $self->type;
        return $self->$type;
    } else {

        #my ( $rel ) =
        #grep { $_->type eq 'many to one' } $self->meta->relationships;
        #die $rel->name;

        #my $master = $rel->name;

        #return $self->$master;
    }
}

1;
