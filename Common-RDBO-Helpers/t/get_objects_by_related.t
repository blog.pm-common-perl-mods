#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 10;

use NewDB;
use Post;
use Post::Manager;
use Tag;

my $db = NewDB->new();

$db->init();

my @data = (
    {
        addtime  => time,
        title    => 'Linux',
        content  => 'blah-blah',
        tags => [ { name => 'linux' }, { name => 'unix' }, { name => 'bsd' }, ]
    },
    {
        addtime => time + 5,
        title   => 'Unix',
        content => 'blah-blah',
        tags    => [ { name => 'linux' }, { name => 'unix' }, ]
    },
    {
        addtime => time + 10,
        title   => 'BSD',
        content => 'blah-blah',
        tags    => [ { name => 'linux' }, ]
    },
);

foreach my $data ( @data ) {
    my $tags     = delete $data->{ tags };

    my $post = Post->new( %$data );

    $post->tags( $tags );

    $post->save( cascade => 1 );
}

my $posts_with_unix_tag = Post::Manager->get_objects_by_related(
    rel_name    => 'tags',
    rel_objects => [2]
);

is( @$posts_with_unix_tag, 2 );
is( $posts_with_unix_tag->[0]->title, 'Linux');
is( $posts_with_unix_tag->[1]->title, 'Unix');


my $posts_with_linux_tag = Post::Manager->get_objects_by_related(
    rel_name    => 'tags',
    rel_objects => 1
);

is( @$posts_with_linux_tag, 3 );
is( $posts_with_linux_tag->[0]->title, 'Linux');
is( $posts_with_linux_tag->[1]->title, 'Unix');
is( $posts_with_linux_tag->[2]->title, 'BSD');


my $posts_with_unix_and_bsd_tag = Post::Manager->get_objects_by_related(
    rel_name    => 'tags',
    rel_objects => [2,3]
);

is( @$posts_with_unix_and_bsd_tag, 2 );
is( $posts_with_unix_and_bsd_tag->[0]->title, 'Linux');
is( $posts_with_unix_and_bsd_tag->[1]->title, 'Unix');


my $posts = Post::Manager->get_objects();
$_->delete(cascade => 1 ) foreach @$posts;
