#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 2;

use NewDB;
use Post;
use Post::Manager;
use Tag;
use Tag::Manager;

my $db = NewDB->new();

$db->init();

my @data = (
    {
        addtime  => time,
        title    => 'Linux',
        content  => 'blah-blah',
        tags => [ { name => 'linux' }, { name => 'unix' }, { name => 'bsd' }, ]
    },
    {
        addtime => time + 5,
        title   => 'Unix',
        content => 'blah-blah',
        tags    => [ { name => 'linux' }, { name => 'unix' }, ]
    },
    {
        addtime => time + 10,
        title   => 'BSD',
        content => 'blah-blah',
        tags    => [ { name => 'linux' }, ]
    },
);

foreach my $data ( @data ) {
    my $tags     = delete $data->{ tags };

    my $tag = Post->new( %$data );

    $tag->tags( $tags );

    $tag->save( cascade => 1 );
}

my $tags = Tag::Manager->get_tags();

is( scalar @$tags, 3 );

$cloud = Tag::Manager->get_objects_cloud(
    rel_name      => 'post_tag_map',
    active_column => 'name',
    count_method  => 'count'
);

is_deeply(
    $cloud,
    [
        {
            'level' => 0,
            'count' => 1,
            'name'  => 'bsd',
            'id'    => '3'
        },
        {
            'level' => 3,
            'count' => 3,
            'name'  => 'linux',
            'id'    => '1'
        },
        {
            'level' => 1,
            'count' => 2,
            'name'  => 'unix',
            'id'    => '2'
        }

    ]
);

my $posts = Post::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$posts;
