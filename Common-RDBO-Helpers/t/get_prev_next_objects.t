#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 8;

use NewDB;
use Post;
use Post::Manager;

my $db = NewDB->new();

$db->init();

my @data = (
    {
        addtime => time,
        title   => 'Linux',
        content => 'blah-blah',
    },
    {
        addtime => time + 5,
        title   => 'Unix',
        content => 'blah-blah',
    },
    {
        addtime => time + 10,
        title   => 'BSD',
        content => 'blah-blah',
    },
);

foreach my $data ( @data ) {
    my $post = Post->new( %$data );

    $post->save( cascade => 1 );
}

my $posts = Post::Manager->get_posts();

is( scalar @$posts, 3 );

foreach my $post ( @$posts ) {
    ok( $post->title );

    if ( $post->title eq 'Linux' ) {
        is( @{ $post->get_prev_objects( column => 'addtime', limit  => 1) }, 0);
    } elsif ( $post->title eq 'Unix' ) {
        is( @{ $post->get_prev_objects( column => 'addtime', limit  => 1) }, 1);
        is( @{ $post->get_next_objects( column => 'addtime', limit  => 1) }, 1);
    } elsif ( $post->title eq 'BSD' ) {
        is( @{ $post->get_next_objects( column => 'addtime', limit  => 1) }, 0);
    }
}

my $posts = Post::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$posts;
