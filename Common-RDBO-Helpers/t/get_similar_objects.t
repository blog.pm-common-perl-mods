#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 3;

use NewDB;
use Post;
use Post::Manager;
use Tag;

my $db = NewDB->new();

$db->init();

my @data = (
    {
        addtime  => time,
        title    => 'Linux',
        content  => 'blah-blah',
        tags => [ { name => 'linux' }, { name => 'unix' }, { name => 'bsd' }, ]
    },
    {
        addtime => time + 5,
        title   => 'Unix',
        content => 'blah-blah',
        tags    => [ { name => 'linux' }, { name => 'unix' }, ]
    },
    {
        addtime => time + 10,
        title   => 'BSD',
        content => 'blah-blah',
        tags    => [ { name => 'bsd' }, ]
    },
);

foreach my $data ( @data ) {
    my $tags     = delete $data->{ tags };

    my $post = Post->new( %$data );

    $post->tags( $tags );

    $post->save( cascade => 1 );
}

my $post = Post->new( id => 3 );
$post->load();

is($post->title, 'BSD');
is(@{ $post->tags }, 1 );

my $similar_posts = Post::Manager->get_similar_objects(
    object      => $post,
    rel_name    => 'tags'
);

is(@$similar_posts, 1);

my $posts = Post::Manager->get_objects();
$_->delete(cascade => 1 ) foreach @$posts;
