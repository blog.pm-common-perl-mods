#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 3;

use NewDB;
use Post;
use Post::Manager;
use DateTime;

my $db = NewDB->new();

$db->init();

foreach my $i ( 1 .. 25 ) {
    my %data = (
        addtime => time,
        title   => 'foo',
        content => 'bar'
    );

    my $post = Post->new( %data );
    $post->save( cascade => 1 );
}

my $pager;

$pager = Post::Manager->get_objects_pager(page => 'abc');

is_deeply(
    $pager,
    {
        total     => 25,
        per_page => 20,
        page      => 1
    }
);

$pager = Post::Manager->get_objects_pager(page => 3);

is_deeply(
    $pager,
    {
        total     => 25,
        per_page => 20,
        page      => 1
    }
);

$pager = Post::Manager->get_objects_pager(page => 2);

is_deeply(
    $pager,
    {
        total     => 25,
        per_page => 20,
        page      => 2
    }
);

my $posts = Post::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$posts;
