#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 8;

use NewDB;
use Post;
use Post::Manager;
use DateTime;

my $db = NewDB->new();

$db->init();

my @data = (
    {
        addtime => DateTime->new( year => 2008, month => 8, day => 31 )->epoch,
        title   => 'Linux',
        content => 'blah-blah'
    },
    {
        addtime => DateTime->new( year => 2008, month => 8, day => 31 )->epoch,
        title   => 'Unix',
        content => 'blah-blah'
    },
    {
        addtime => DateTime->new( year => 2008, month => 7, day => 21 )->epoch,
        title   => 'Unix',
        content => 'blah-blah'
    },
    {
        addtime => DateTime->new( year => 2008, month => 7, day => 31 )->epoch,
        title   => 'BSD',
        content => 'blah-blah'
    },
    {
        addtime => DateTime->new( year => 2008, month => 5, day => 1 )->epoch,
        title   => 'BSD',
        content => 'blah-blah'
    },
    {
        addtime => DateTime->new( year => 2007, month => 2, day => 3 )->epoch,
        title   => 'BSD',
        content => 'blah-blah'
    },
    ,
);

foreach my $data ( @data ) {
    my $post = Post->new( %$data );

    $post->save( cascade => 1 );
}

my $archive;

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2006 );
is( @$archive, 0 );

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2007 );
is( @$archive, 1 );

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2008 );
is( @$archive, 5 );

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2008, month => 8 );
is( @$archive, 2 );

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2008, month => 8, day => 31 );
is( @$archive, 2 );

$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2008, month => 7, day => 21 );
is( @$archive, 1 );
$archive = Post::Manager->get_objects_archive( column => 'addtime', year => 2008, month => 8, day => 31 );
is( @$archive, 2 );

my $month_list =
  Post::Manager->get_objects_archive_month_list( column => 'addtime' );

is_deeply(
    $month_list,
    [
        {
            count      => 2,
            year       => 2008,
            month      => 8,
            month_name => 'August'
        },
        {
            count      => 2,
            year       => 2008,
            month      => 7,
            month_name => 'July'
        },
        {
            count      => 1,
            year       => 2008,
            month      => 5,
            month_name => 'May'
        },
        {
            count      => 1,
            year       => 2007,
            month      => 2,
            month_name => 'February'
        }
    ]
);

my $posts = Post::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$posts;
