use strict;
use Test::More tests => 2;

BEGIN { use_ok( 'Common::RDBO::Helpers' ) }
BEGIN { use_ok( 'Common::RDBO::Helpers::Manager' ) }
