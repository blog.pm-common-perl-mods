#! /usr/bin/perl

use lib 't/lib';

use Test::More 'tests' => 7;

use NewDB;
use Post;
use Post::Manager;
use Article;
use Article::Manager;
use Comment;
use Comment::Manager;
use DateTime;

my $db = NewDB->new();

$db->init();

my @posts = (
    {
        addtime  => time,
        title    => 'Linux',
        content  => 'blah-blah',
        comments => [
            {
                addtime => time,
                type    => 'post',
                name    => 'anon',
                content => 'hallo'
            }
        ],
    }
);

my @articles = (
    {
        addtime  => time,
        title    => 'FreeBSD',
        content  => 'blah-blah',
        comments => [
            {
                addtime => time,
                type    => 'article',
                name    => 'bar',
                content => 'foo'
            }
        ],
    }
);

foreach my $post ( @posts ) {
    my $comments = delete $post->{ comments };

    my $post = Post->new( %$post );

    $post->comments( $comments );

    $post->save( cascade => 1 );
}

foreach my $article ( @articles ) {
    my $comments = delete $article->{ comments };

    my $article = Article->new( %$article );

    $article->comments( $comments );

    $article->save( cascade => 1 );
}

my $comments = Comment::Manager->get_objects();

is( scalar @$comments, 2 );

my $article_comments =
  Comment::Manager->get_objects( query => [ type => 'article' ] );

is( scalar @$article_comments, 1 );

my $comment = $article_comments->[ 0 ];
is( $comment->type, 'article' );

my $master = $comment->get_master_object();
ok( $master );

is( $master->title, 'FreeBSD' );

$comment = Comment->new(master_id => 1, type => 'article');
$comment->load();
is($comment->type, 'article');
is($comment->content, 'foo');

my $articles = Article::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$articles;

my $posts = Post::Manager->get_objects();
$_->delete( cascade => 1 ) foreach @$posts;
