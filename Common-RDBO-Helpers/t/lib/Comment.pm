package Comment;

use strict;

use base qw(DB::Object);

use Common::RDBO::Helpers ':all';

__PACKAGE__->meta->setup(
    table => 'comment',

    columns => [
        qw/
          master_id
          type
          addtime
          user_id
          email
          name
          url
          content
          /
    ],

    primary_key_columns => [ qw/ master_id type / ],

    relationships => [
        post => {
            type        => 'many to one',
            class       => 'Post',
            key_columns => { master_id => 'id' }
        },
        article => {
            type        => 'many to one',
            class       => 'Article',
            key_columns => { master_id => 'id' }
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
