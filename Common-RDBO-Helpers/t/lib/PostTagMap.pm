package PostTagMap;

use strict;

use base qw(DB::Object);

__PACKAGE__->meta->setup(
    table => 'post_tag_map',

    columns => [ qw/ post_id tag_id / ],

    primary_key_columns => [ 'post_id', 'tag_id' ],

    foreign_keys => [
        post => {
            class       => 'Post',
            key_columns => { post_id => 'id' }
        },

        tag => {
            class       => 'Tag',
            key_columns => { tag_id => 'id' }
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
