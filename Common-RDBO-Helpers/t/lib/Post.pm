package Post;

use strict;

use base qw(DB::Object);

use Rose::Object::MakeMethods::Generic (
    scalar => [ qw/ photo photo_count comment_count / ] );

use Common::RDBO::Helpers qw(:all);

__PACKAGE__->meta->setup(
    table => 'post',

    columns => [
        qw/ id title content /,
        addtime => { type => 'epoch' }
    ],

    primary_key_columns => [ qw/ id / ],

    relationships => [
        tags => {
            type      => 'many to many',
            map_class => 'PostTagMap',
            map_from  => 'post',
            map_to    => 'tag'
        },
        post_tag_map => {
            type  => 'one to many',
            class => 'PostTagMap',
        },
        comments => {
            type       => 'one to many',
            class      => 'Comment',
            column_map => { id => 'master_id' },
            query_args => [ type => 'post' ]
        },
        photos => {
            type       => 'one to many',
            class      => 'Photo',
            column_map => { id => 'master_id' },
            query_args => [ type => 'post' ]
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
