package Photo;

use strict;

use base qw(DB::Object);

use Common::RDBO::Helpers ':all';

use Rose::Object::MakeMethods::Generic ( scalar => [ qw/ photo_count / ] );

__PACKAGE__->meta->setup(
    table => 'photo',

    columns => [
        qw/
          id
          addtime
          master_id
          type
          user_id
          photo
          /,
          poster => { default => 0 }
    ],

    primary_key_columns => [ 'id' ],

    relationships => [
        post => {
            type        => 'many to one',
            class       => 'Post',
            key_columns => { master_id => 'id' }
        },
        article => {
            type        => 'many to one',
            class       => 'Article',
            key_columns => { master_id => 'id' }
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
