package Tag;

use strict;

use base qw(DB::Object);

use Rose::Object::MakeMethods::Generic (
    scalar => [ qw/ count / ] );

use Common::RDBO::Helpers ':all';

__PACKAGE__->meta->setup(
    table => 'tag',

    columns => [ qw/ id name / ],

    primary_key_columns => [ 'id' ],

    unique_key => 'name',

    relationships => [
        post_tag_map => {
            type       => 'one to many',
            class      => 'PostTagMap',
            column_map => { id => 'tag_id' }
        },
        posts => {
            type      => 'many to many',
            map_class => 'PostTagMap',
            map_from  => 'tag',
            map_to    => 'post'
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
