package Comment::Manager;

use strict;

use base 'Rose::DB::Object::Manager';

sub object_class { 'Comment' }

__PACKAGE__->make_manager_methods( 'comments' );

use Common::RDBO::Helpers ':all';

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
