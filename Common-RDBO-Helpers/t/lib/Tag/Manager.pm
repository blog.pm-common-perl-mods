package Tag::Manager;

use strict;

use base 'Rose::DB::Object::Manager';

use Common::RDBO::Helpers::Manager qw(:all);

sub object_class { 'Tag' }

__PACKAGE__->make_manager_methods( 'tags' );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
