package Article::Manager;

use strict;

use base 'Rose::DB::Object::Manager';

use Common::RDBO::Helpers::Manager ':all';

sub object_class { 'Article' }

__PACKAGE__->make_manager_methods( 'articles' );

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
