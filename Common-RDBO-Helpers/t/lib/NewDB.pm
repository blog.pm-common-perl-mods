package NewDB;

use DB;

sub new {
    my $class = shift;

    my $self = { db => DB->new };

    bless $self, $class;

    return $self;
}

sub db { shift->{ db } }

sub exists {
    my $self = shift;

    return -f $self->db->database && @{ $self->db->list_tables };
}

sub cleanup {
    my $self = shift;

    unlink $self->db->database;
}

sub init {
    my $self = shift;

    my $dbh = $self->db->retain_dbh;

    unless ( $self->exists ) {
        warn 'Creating new db...';

        $dbh->do( <<SQL );
CREATE TABLE `post` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `addtime` INTEGER NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `photo` VARCHAR(255),
  `content` VARCHAR(255) NOT NULL
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE `article` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `addtime` INTEGER NOT NULL,
  `photo` VARCHAR(255),
  `title` VARCHAR(255) NOT NULL,
  `content` VARCHAR(255) NOT NULL
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE tag (
  id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR NOT NULL,
  UNIQUE(name)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE post_tag_map (
  post_id INTEGER KEY NOT NULL,
  tag_id INTEGER NOT NULL,
  PRIMARY KEY(post_id, tag_id)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE comment (
  master_id INTEGER NOT NULL,
  type VARCHAR,
  user_id INTEGER,
  `addtime` INTEGER NOT NULL,
  name VARCHAR,
  url VARCHAR,
  email VARCHAR,
  content TEXT NOT NULL,
  PRIMARY KEY(master_id, type)
);
SQL

        $dbh->do( <<SQL );
CREATE TABLE photo (
  id INTEGER PRIMARY KEY NOT NULL,
  master_id INTEGER NOT NULL,
  poster INTEGER(1) NOT NULL,
  type VARCHAR,
  user_id INTEGER,
  `addtime` INTEGER NOT NULL,
  photo VARCHAR
);
SQL

        $dbh->disconnect();
    }
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
