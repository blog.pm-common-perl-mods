package Photo::Manager;

use strict;

use base 'Rose::DB::Object::Manager';

sub object_class { 'Photo' }

__PACKAGE__->make_manager_methods( 'photos' );

use Common::RDBO::Helpers::Manager ':all';

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
