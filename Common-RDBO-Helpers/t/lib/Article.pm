package Article;

use strict;

use base qw(DB::Object);

use Rose::Object::MakeMethods::Generic (
    scalar => [ qw/ photo_count comment_count / ] );

__PACKAGE__->meta->setup(
    table => 'article',

    columns => [
        qw/ id title content photo /,
        addtime => { type => 'epoch' }
    ],

    primary_key_columns => [ qw/ id / ],

    relationships => [
        comments => {
            type       => 'one to many',
            class      => 'Comment',
            column_map => { id => 'master_id' },
            query_args => [ type => 'article' ]
        },
        photos => {
            type       => 'one to many',
            class      => 'Photo',
            column_map => { id => 'master_id' },
            query_args => [ type => 'article' ]
        }
    ]
);

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
