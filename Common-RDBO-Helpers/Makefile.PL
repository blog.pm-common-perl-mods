use inc::Module::Install;

name 'Common-RDBO-Helper';

all_from 'lib/Common/RDBO/Helpers.pm';

requires(
    'Rose::DB::Object' => '',
    'Hash::Merge'      => '',
);

auto_install;

WriteAll;
