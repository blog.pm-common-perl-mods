use strict;
use Test::More tests => 3;

use IO::File;
use Form::Processor::Field::File;

my $field = Form::Processor::Field::File->new(
    name => 'test_field',
    size => 2000,
    type => 'file',
    mime => 'text/plain',
    form => undef,
);

ok( defined $field, 'new() called' );

$field->input( 'blah' );
$field->validate_field;
ok( $field->has_error );

my $fh = IO::File->new();

$fh->open( "< t/03.t" );

$field->input( $fh );
$field->validate_field;
ok( !$field->has_error );

$fh->close();
