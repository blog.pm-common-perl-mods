package Form::Processor::Field::File;
use strict;
use warnings;
use base 'Form::Processor::Field';
our $VERSION = '0.01';

use File::MimeInfo::Magic ();
use Fcntl 'SEEK_SET';

use Rose::Object::MakeMethods::Generic(
    scalar => [
        size => { interface => 'get_set_init' },
        mime => { interface => 'get_set_init' },
    ],
);

sub init_size { 1 }
sub init_mime { }

sub validate {
    my $self = shift;

    return unless $self->SUPER::validate;

    return $self->add_error( 'Wrong internal data provided' )
      unless $self->input;

    my $fh = $self->input;

    return $self->add_error( 'Wrong file' ) unless ref $fh;

    return $self->add_error( 'File does not exist' ) unless -f $fh;

    if ( my $size = $self->size ) {
        return $self->add_error( 'Wrong file size' )
          if ( stat $fh )[ 7 ] > $size;
    }

    if ( my $mimetype = $self->mime ) {
        return $self->add_error( 'Wrong file type' )
          unless File::MimeInfo::Magic::mimetype( $fh ) =~ $mimetype;
    }

    if ( ref $fh eq 'GLOB' ) {
        seek( $fh, 0, SEEK_SET );    # seek offset
    } else {                         # allowing for IO::Something
        $fh->seek( 0, SEEK_SET );    # seek offset
    }

    return 1;
}

=head1 AUTHORS

vti

=head1 COPYRIGHT

This library is free software, you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

1;
