package CatalystX::Plugin::Static::Simple::ThumbnailsGenerator;

use warnings;
use strict;

use base 'Catalyst::Plugin::Static::Simple';

use Common::Image::Util;

our $VERSION = '0.01';

sub _locate_static_file {
    my $c = shift;
    my ( $req_path ) = @_;

    my $rs = $c->NEXT::_locate_static_file( @_ );

    unless ( $rs ) {
        my $generator = $c->config->{ static }->{ thumbnails_generator };

        if ( $generator ) {
            foreach my $conf ( @$generator ) {
                my $path = $conf->{ path };
                my $namespace = $conf->{ namespace };

                if ( $namespace ) {
                    $namespace =~ s/\/+$//;
                    $path = $namespace . '/' . $path;
                }

                # from Catalyst::Plugin::Static::Simple
                $path =~ s{/$}{};

                if ( $req_path =~ m/^$path\/(.*?)\// ) {
                    my $thumb_geometry = $1;

                    my $geometry = $conf->{ geometry };

                    return
                      unless grep { $thumb_geometry eq $_ }
                      keys %$geometry;

                    my $thumb_fullpath;
                    my $image_fullpath;
                    my $include_path = $c->config->{ static }->{ include_path };

                    foreach my $dir (
                        ref $include_path ? @$include_path : $include_path )
                    {
                        $thumb_fullpath =
                          Path::Class::File->new( split( ',', $dir ),
                            $req_path );

                        ( $image_fullpath ) = ( $req_path =~ m/^$path\/(.*)/ );
                        $image_fullpath = $namespace . '/' . $image_fullpath
                          if $namespace;
                        $image_fullpath =~ s/$thumb_geometry\///;
                        $image_fullpath = $dir . '/' . $image_fullpath;
                        #$c->log->debug( 'image_fullpath=' . $image_fullpath );

                        if ( -f $image_fullpath ) {
                            last;
                        } else {
                            $image_fullpath = undef;
                            next;
                        }
                    }

                    return unless $image_fullpath;

                    my $u = Common::Image::Util->new();

                    eval {
                        $u->thumbnail(
                            image_fullpath => $image_fullpath,
                            thumb_fullpath => $thumb_fullpath,
                            thumb_geometry => $geometry->{ $thumb_geometry }
                        );
                    };

                    if ( $@ ) {
                        $c->log->debug( $@ );
                        return;
                    } else {
                        return $c->_static_file( $thumb_fullpath );
                    }
                }
            }
        }
    }

    return $rs;
}

=head1 AUTHOR

vti

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
