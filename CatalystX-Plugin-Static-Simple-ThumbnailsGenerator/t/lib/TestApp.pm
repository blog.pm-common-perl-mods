package TestApp;

use strict;
use Catalyst;
use File::Spec::Functions;
use FindBin;

our $VERSION = '0.01';

TestApp->config(
    name     => 'TestApp',
    debug    => 1,
    static => {
        thumbnails_generator => [
            {
                path      => 'thumbs',
                namespace => 'static',
                geometry =>
                  { small => '100x', normal => '200x', large => '400x' }
            }
        ]
    }
);

my @plugins = qw/ +CatalystX::Plugin::Static::Simple::ThumbnailsGenerator /;

TestApp->setup( @plugins );

sub default : Private {
    my ( $self, $c ) = @_;

    $c->res->output( 'default' );
}

1;
