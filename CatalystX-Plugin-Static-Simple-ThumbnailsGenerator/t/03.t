use Test::More tests => 8;

use FindBin;
use lib "$FindBin::Bin/lib";

use Catalyst::Test 'TestApp';

ok( my $res = request('http://localhost/static/linux.png'), 'request ok' );
is( $res->content_type, 'image/png', 'content-type image/png ok' );

ok( my $res = request( 'http://localhost/static/thumbs/small/linux.png' ),
    'request ok' );

my $file = 't/lib/TestApp/root/static/thumbs/small/linux.png';
ok( -f $file );

ok( my $res = request( 'http://localhost/static/thumbs/small/linux.png' ),
    'request ok' );
ok( -f $file );

unlink $file;

ok( my $res = request( 'http://localhost/static/thumbs/smalla/linux.png' ),
    'request not ok' );
like( $res->content, qr/default/, '' );
